package com.project.brokerzeye;

/**
 * Created by phong on 9/17/2015.
 */

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.project.brokerzeye.adapter.DialogAdapter;
import com.project.brokerzeye.adapter.UserAdapter;
import com.project.brokerzeye.data.DatabaseHandler;
import com.project.brokerzeye.quickbloxtest.core.ChatService;
import com.project.brokerzeye.utils.Const;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InBoxFragment extends Fragment {
    SwipeRefreshLayout swipeRefreshLayout;
    ListView listViewDialog;
    ProgressBar progressBar;

    EditText postAdd;

    FloatingActionButton createAddListing;
    private TabActivity tabActivity;
    DatabaseHandler databaseHandler;
    MaterialDialog dialog;
    private UserAdapter usersAdapter;
    private List<QBUser> users = new ArrayList<QBUser>();
    public InBoxFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabActivity = (TabActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        listViewDialog = (ListView) rootView.findViewById(R.id.listViewDialogs);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        createAddListing=(FloatingActionButton) rootView.findViewById(R.id.createAddListing);

        databaseHandler = new DatabaseHandler(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        dialog = new MaterialDialog.Builder(getActivity())
                .title(null)
                .content("Wating")
                .progress(true, 0).build();
        dialog.setCancelable(false);
        ChatService.initIfNeed(getActivity());


        if (tabActivity.isSessionActive()) {
            Log.e("session", "active");
            getDialogs();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getRefreshDialogs();

                }
            });
        }
        createAddListing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                submitAdd();
            }
        });
        return rootView;
    }

    private void getDialogs() {
        progressBar.setVisibility(View.VISIBLE);
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
   //     requestBuilder.eq("zoneid", "6");
       // requestBuilder.addParameter("data[class_name]", "Advert");
       // requestBuilder.addParameter("data[title]", "bingo");
        // Get dialogs
        //

        requestBuilder.addRule("zoneid",
                "eq",
                "2");

        ChatService.getInstance().getDialogs(new QBEntityCallbackImpl() {
            @Override
            public void onSuccess(Object object, Bundle bundle) {
                progressBar.setVisibility(View.GONE);

                 final ArrayList<QBDialog> dialogs = (ArrayList<QBDialog>) object;
                ArrayList<QBDialog> customizedDialogs= new ArrayList<QBDialog>();  ;//=  dialogs;
                // build list view
                //
                for (int i = 0; i < dialogs.size(); i++) {
                    /*if(dialogs.get(i).getData().get("zoneid").equals("2"))
                    customizedDialogs.add(dialogs.get(i));*/
                }


                buildListView(dialogs);
            }

            @Override
            public void onError(List errors) {
                progressBar.setVisibility(View.GONE);
                Log.e("error", "getdialog");
               /* AlertDialog.Builder dialogs = new AlertDialog.Builder(getActivity());
                dialogs.setMessage("Chat login errors: " + errors.get(0).toString()).create().show();
                Intent init = new Intent(getActivity(),Splash.class);
                startActivity(init);
                getActivity().finish();*/


            }
        });
    }

    private void getRefreshDialogs() {


        // Get dialogs
        //
        Map<String, String> data = new HashMap<String, String>();
        data.put("data[class_name]", "Advert");
        data.put("data[title]", "bingo");

        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();

        requestBuilder.setPagesLimit(100);

        QBChatService.getChatDialogs(null, requestBuilder, new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBDialog> dialogs, Bundle args) {

            }

            @Override
            public void onError(List<String> errors) {

            }
        });
        ChatService.getInstance().getDialogs(new QBEntityCallbackImpl() {
            @Override
            public void onSuccess(Object object, Bundle bundle) {
                progressBar.setVisibility(View.GONE);

                final ArrayList<QBDialog> dialogs = (ArrayList<QBDialog>) object;
                swipeRefreshLayout.setRefreshing(false);

                // build list view
                //
                buildListView(dialogs);
            }

            @Override
            public void onError(List errors) {
                progressBar.setVisibility(View.GONE);
                Log.e("error", "getdialog");
                AlertDialog.Builder dialogs = new AlertDialog.Builder(getActivity());
                dialogs.setMessage("Chat login errors: " + errors.get(0).toString()).create().show();


            }
        });
    }

    void buildListView(List<QBDialog> dialogs) {
        final DialogAdapter adapter;
        try {
            adapter = new DialogAdapter(dialogs, getActivity());
            listViewDialog.setAdapter(adapter);

            // choose dialog
            //
            listViewDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    QBDialog selectedDialog = (QBDialog) adapter.getItem(position);

                    Bundle bundle = new Bundle();
                   // bundle.putSerializable(ChatActivity.EXTRA_DIALOG, selectedDialog);

                    // Open chat activity
                 //   ChatActivity.start(getActivity(), bundle);
                    getActivity().finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void submitAdd() {
        // TODO Auto-generated method stub
        boolean wrapInScrollView = true;
        MaterialDialog submitFeedbackDialog = new MaterialDialog.Builder(getActivity())
                .theme(Theme.LIGHT)
                .title("Post Your Ad")
                .positiveText("Submit")
                .negativeText("Cancel")
                .negativeColorRes(R.color.button_blue_normal_color)
                .positiveColorRes(R.color.button_blue_pressed_color)
                .customView(R.layout.change_password_dialog, wrapInScrollView)
                .callback(new MaterialDialog.ButtonCallback() {


                    @Override
                    public void onPositive(MaterialDialog materialDialog) {


                        String postAdds = postAdd.getText().toString();



                        if ((postAdds.length() <= 8)) {

                              Toast.makeText(getActivity(), "Please enter more than 10 characters", Toast.LENGTH_LONG).show();
                        } else {
                            //Toast.makeText(getActivity(), "Password must be more than 5 characters long.", Toast.LENGTH_LONG).show();

                           /* prevmessage = messageView.getText().toString();
                            prevtitle = titleView.getText().toString();*/
                            CreateAdListingAsGroupChat(postAdd.getText().toString());

                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog materialDialog) {

                    }
                })
                .build();

        View submitView = submitFeedbackDialog.getCustomView();
        postAdd = (EditText) submitView
                .findViewById(R.id.postAdd);

        /**
         * Don't understand
         */
       /* messageView.setText(prevmessage);
        titleView.setText(prevtitle);*/

       /* serviceView = (CustomMaterialEditText) submitView.findViewById(R.id.category_view);
        serviceView.setFocusable(false);
        serviceView.setFocusableInTouchMode(false);
        serviceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServiceDialog();
            }
        });*/

        submitFeedbackDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public void CreateAdListingAsGroupChat(String dialogName) {

        int a1[] = { ChatService.getInstance().getCurrentUser().getId()};
        ArrayList<Integer> arl = new ArrayList<Integer>();
        for (int i : a1) {
            arl.add(i);
            Map<String, String> data = new HashMap<String, String>();
            data.put("data[class_name]", "zoneinfo");
            data.put("data[zoneid]", Const.zoneId);


            QBDialog dialogToCreate = new QBDialog();
            dialogToCreate.setName(dialogName);
            dialogToCreate.setData(data);
            //ChatService.getInstance().addDialogsUsers();
            dialogToCreate.setType(QBDialogType.PUBLIC_GROUP);
            dialogToCreate.setOccupantsIds(arl);
            dialogToCreate.setName(dialogName);

            QBChatService.getInstance().getGroupChatManager().createDialog(dialogToCreate, new QBEntityCallbackImpl<QBDialog>() {
                @Override
                public void onSuccess(QBDialog dialog, Bundle args) {

                    startGroupChat(dialog);

                }

                @Override
                public void onError(List<String> errors) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setMessage("dialog creation errors: " + errors).create().show();
                }
            });
            //   Toast.makeText(getApplicationContext(), "Select only one to chat", Toast.LENGTH_SHORT).show();
        }
    }
    private void startGroupChat(QBDialog dialog){
        Bundle bundle = new Bundle();
     //   bundle.putSerializable(ChatActivity.EXTRA_DIALOG, dialog);

       // ChatActivity.start(getActivity(), bundle);
    }


}