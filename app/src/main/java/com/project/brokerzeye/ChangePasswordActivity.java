package com.project.brokerzeye;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class ChangePasswordActivity extends AppCompatActivity implements TextWatcher {

	private EditText phnnumber_edt, newpass_edt;
	private String appkey = "", phonenmuber_str = "";
	private Spinner country_code_edt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changepassword);

		getSupportActionBar().hide();
		appkey = getIntent().getStringExtra("appkey");
		phonenmuber_str = getIntent().getStringExtra("phonenmuber");

		String password_str = "<font color='#000000'>New Password </font>" + "<font color='#FF0000'>*</font>";
		String phone_str = "<font color='#000000'>Mobile Number </font>" + "<font color='#FF0000'>*</font>";

		phnnumber_edt = (EditText)findViewById(R.id.phnnumber_edt);
		phnnumber_edt.setHint(Html.fromHtml(phone_str));
		phnnumber_edt.addTextChangedListener(this);
		newpass_edt = (EditText)findViewById(R.id.newpass_edt);
		newpass_edt.setHint(Html.fromHtml(password_str));
		newpass_edt.addTextChangedListener(this);
		country_code_edt = (Spinner)findViewById(R.id.country_code_edt);
		country_code_edt.setEnabled(false);

		ArrayList<String> country_code_arr = new ArrayList<String>(
				Arrays.asList(getResources().getStringArray(R.array.country_code_arr)));
		phnnumber_edt.setText(phonenmuber_str.substring(3, phonenmuber_str.length()));
		country_code_edt.setSelection(country_code_arr.indexOf(phonenmuber_str.substring(0, 2)));

		Button submit_btn = (Button)findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(ChangePasswordActivity.this)){
					String phnnumber_str = phnnumber_edt.getText().toString().trim();
					String new_password_str = newpass_edt.getText().toString().trim();
					if(!Util.ValidatePhoneNumber(phnnumber_str))
						phnnumber_edt.setError("please enter valid phone number");
					else if(!Util.PASSWORD_PATTERN.matcher(new_password_str).matches())
						newpass_edt.setError("enter password with min 8 characters and capital, numerical and special characters");
					else{
						phnnumber_str = country_code_edt.getSelectedItem().toString()+phnnumber_str;
						JSONObject obj = new JSONObject();
						try {
							obj.put("phoneNumber", phnnumber_str);
							obj.put("newPassword", new_password_str);
							obj.put("appkey", appkey);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("Please wait...", ChangePasswordActivity.this,
								Util.CHANGE_PASSWORD_API, obj);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("change pass---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									String res_code = res_obj.getString("code");
									String message = res_obj.getString("message");
									if(res_code.equals("200")){
										finish();
									}
									Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_SHORT).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(ChangePasswordActivity.this, "Something went wrong. Please try again",
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(ChangePasswordActivity.this, "Please enable internet connection",
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		phnnumber_edt.setError(null);
		newpass_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
								  int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

}
