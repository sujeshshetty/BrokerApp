package com.project.brokerzeye;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.brokerzeye.api.PostApi;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBHelper;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.request.LoginResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class SplashActivity extends Activity {

	private String regId = "";
	private GoogleCloudMessaging gcm;
	public static final String EXTRA_MESSAGE = "message";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN );
		setContentView(R.layout.activity_splash);

		DBHelper db = new DBHelper(this);
		db.open();
		db.close();

		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regId = Util.getRegistrationId(this);

			if (regId.isEmpty()) {
				registerInBackground();
			}
			else
				checkProfession();
		} else {
			Log.i("Demo", "No valid Google Play Services APK found.");
		}
	}

	private void gotoNextScreen(long delay){
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
				dbq.open();
				if(dbq.checkLogin())
                    LoginApi();
               // startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
				else{
					if(Util.readPreference(SplashActivity.this, "forgot_pass").equals("1"))
						startActivity(new Intent(SplashActivity.this, ChangePasswordActivity.class));
					else
						startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
				}
                dbq.close();

			}
		}, delay);
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("Demo", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private void registerInBackground() {

		new GetRegistrationId().execute();
	}

	private class GetRegistrationId extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(SplashActivity.this);
				}
				regId = gcm.register(Util.SENDER_ID);
				msg = "Device registered, registration ID=" + regId;

				storeRegistrationId(SplashActivity.this, regId);
				DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
				dbq.open();
				if(dbq.checkLogin()){
					String phnnumber = dbq.getPhoneNumber();
					String appkey = dbq.getAppKey();
					String url = Util.UPDATE_GCM_API+"?phoneNumber="+phnnumber+"&appkey="+appkey+
							"&regId="+regId;
					String up_result = Connection.serverCallGet(url);
					Log.v("up_result---", up_result+"");
				}
				dbq.close();
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
			}

			return msg;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			checkProfession();
		}
	}

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = Util.getGCMPreferences(context);
		int appVersion = Util.getAppVersion(context);
		Log.i("Demo", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Util.PROPERTY_REG_ID, regId);
		editor.putInt(Util.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	private void checkProfession(){
		GenericAsynTask task = new GenericAsynTask("", this, Util.ALL_PROFESSION_API, null);
		task.setOnTaskListener(new GenericAsynTask.TaskListener() {

			@Override
			public void onSuccess(String success) {
				// TODO Auto-generated method stub
				try {
					JSONObject obj = new JSONObject(success);
					JSONObject status_obj = obj.getJSONObject("status");
					if (status_obj.getString("code").equals("200")) {
						DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
						dbq.open();
						dbq.deleteProfession();
						JSONArray professions_arr = obj.getJSONArray("professions");
						for(int i=0;i<professions_arr.length();i++){
							JSONObject profession_obj = professions_arr.getJSONObject(i);
							dbq.insertProfession(profession_obj.getString("Id"),
									profession_obj.getString("professionName"));
						}
						dbq.close();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				gotoNextScreen(1000);
			}

			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				gotoNextScreen(1000);
			}
		});
		task.execute();
	}


	public void LoginApi() {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

			HashMap<String, String> params = new HashMap<String, String>();

			params.put("phoneNumber", App.getPref().getString("phoneNumber"));
			params.put("password", App.getPref().getString("password"));
			params.put("regId", App.getPref().getString("regId"));

			//String hardcodedDomain= "http://172.16.1.94:8080/TelematicsRESTService/services/ServiceProcess/";
			String URL = "http://122.166.212.177:7777/dubaiapp/rest/loginservice/login";//+ Constants.UPLOAD_BILLS;
			Log.i("", "Login  url : " + URL);
			Log.e("", "Login Params: " + new JSONObject(params));
			PostApi api = new PostApi(Request.Method.POST, URL, new JSONObject(params), new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject jsonObject) {

					Log.e("", "Login API RESPONSE RCVD : " + jsonObject);
					try {


						GsonBuilder builder = new GsonBuilder();
						Gson gson = builder.create();
						LoginResponse getTripDetailsResponse = gson.fromJson(String.valueOf(jsonObject), LoginResponse.class);
                   /* JSONObject res_obj = new JSONObject(success);
                    JSONObject status_obj = res_obj.getJSONObject("status");
                    String res_code = status_obj.getString("code");
                    String message = status_obj.getString("message");*/
						Toast.makeText(SplashActivity.this, getTripDetailsResponse.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
						if (getTripDetailsResponse.getStatus().getCode() == 200) {
							//  JSONObject user_obj = res_obj.getJSONObject("user");
							DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
							dbq.open();
							dbq.insertLogin(getTripDetailsResponse.getUser().getPhoneNumber());
							dbq.insertProfile(String.valueOf(getTripDetailsResponse.getUser().getUserId()), getTripDetailsResponse.getUser().getName(),
									getTripDetailsResponse.getUser().getPhoneNumber(), getTripDetailsResponse.getUser().getEmailId(),
									getTripDetailsResponse.getUser().getAppkey());
							dbq.close();
							Intent intent = new Intent(SplashActivity.this, ProfileActivity.class);

							intent.putExtra("LoginResponse",String.valueOf(jsonObject));
							startActivity(intent);
							finish();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError volleyError) {
                    Toast.makeText(getBaseContext(),"Sorry we couldn't log you in, please check your internet connection",Toast.LENGTH_LONG).show();
					Log.e("", "Volley Error Login: " + volleyError);

				}
			});

			App.getVolleyQueue().add(api);

	}
}
