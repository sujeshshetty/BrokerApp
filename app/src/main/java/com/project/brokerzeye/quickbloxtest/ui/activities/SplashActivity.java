package com.project.brokerzeye.quickbloxtest.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import com.project.brokerzeye.R;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.quickbloxtest.core.ChatService;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

import java.util.List;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Login to REST API
        //
        final QBUser user = new QBUser();
        user.setLogin(App.getPref().getString("mobileNumber"));
        user.setPassword(App.getPref().getString("mobileNumber"));

        ChatService.initIfNeed(this);

        ChatService.getInstance().login(user, new QBEntityCallbackImpl() {

            @Override
            public void onSuccess() {
                // Go to Dialogs screen
                //
                Intent intent = new Intent(SplashActivity.this, ZoneActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(List errors) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                if(errors.get(0).toString().contains("already logged")) {
                    Intent intent = new Intent(SplashActivity.this, ZoneActivity.class);
                    startActivity(intent);
                }
             //   dialog.setMessage("chat login errors: " + errors).create().show();
            }
        });
    }
}