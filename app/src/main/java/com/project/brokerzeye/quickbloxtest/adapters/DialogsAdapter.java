package com.project.brokerzeye.quickbloxtest.adapters;

/**
 * Created by igorkhomenko on 9/12/14.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.project.brokerzeye.R;
import com.project.brokerzeye.quickbloxtest.core.ChatService;
import com.project.brokerzeye.quickbloxtest.ui.activities.ChatActivity;
import com.project.brokerzeye.utils.Const;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DialogsAdapter extends BaseAdapter {
    private List<QBDialog> dataSource;
    private LayoutInflater inflater;
    private Context context;
    EditText postAdd;
    QBDialog privateChatDialog;
    public DialogsAdapter(List<QBDialog> dataSource, Activity ctx) {
        this.dataSource = dataSource;
        this.inflater = LayoutInflater.from(ctx);
        this.context=ctx;
    }

    public List<QBDialog> getDataSource() {
        return dataSource;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        // initIfNeed view
        //
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_room, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.roomName);
            holder.lastMessage = (TextView)convertView.findViewById(R.id.lastMessage);
            holder.groupType = (TextView)convertView.findViewById(R.id.textViewGroupType);
            holder.groupType = (TextView)convertView.findViewById(R.id.textViewGroupType);
            holder.roomImage= (ImageView)convertView.findViewById(R.id.roomImage);
            holder.buttonPrivateType= (ImageButton)convertView.findViewById(R.id.buttonPrivateType);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // set data
        //
        QBDialog dialog = dataSource.get(position);
        privateChatDialog=dialog;
        if(dialog.getType().equals(QBDialogType.GROUP)){
            holder.name.setText(dialog.getName());
        }else if(dialog.getType().equals(QBDialogType.PUBLIC_GROUP))
        {
            holder.name.setText(dialog.getName());

    }else{
            // get opponent name for private dialog
            //
            Integer opponentID = ChatService.getInstance().getOpponentIDForPrivateDialog(dialog);
            QBUser user = ChatService.getInstance().getDialogsUsers().get(opponentID);
            if(user != null){
                holder.name.setText(user.getLogin() == null ? user.getFullName() : user.getLogin());
            }
        }
       /* if (dialog.getLastMessage() != null && StickersManager.isSticker(dialog.getLastMessage())) {
            holder.lastMessage.setText("Sticker");
        } else {*/

            holder.lastMessage.setText(dialog.getLastMessage());
        //}
        holder.groupType.setText(dialog.getType().toString());
        holder.roomImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // handle your text view
                submitAdd();
            }
        });
        holder.buttonPrivateType.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // handle your text view
                submitPrivateDialog(privateChatDialog.getUserId());
            }
        });
        return convertView;
    }

    private static class ViewHolder{
        TextView name;
        TextView lastMessage;
        TextView groupType;
       ImageButton buttonPrivateType;
        ImageView roomImage;
    }

    public void CreateAdListingAsGroupChat(String dialogName) {

        int a1[] = { ChatService.getInstance().getCurrentUser().getId()};
        ArrayList<Integer> arl = new ArrayList<Integer>();
        for (int i : a1) {
            arl.add(i);
            Map<String, String> data = new HashMap<String, String>();
            data.put("data[class_name]", "zoneinfo");
            data.put("data[zoneid]", Const.zoneId);


            QBDialog dialogToCreate = new QBDialog();
            dialogToCreate.setName(dialogName);
            dialogToCreate.setData(data);
            //ChatService.getInstance().addDialogsUsers();
            dialogToCreate.setType(QBDialogType.PUBLIC_GROUP);
            dialogToCreate.setOccupantsIds(arl);
            dialogToCreate.setName(dialogName);

            QBChatService.getInstance().getGroupChatManager().createDialog(dialogToCreate, new QBEntityCallbackImpl<QBDialog>() {
                @Override
                public void onSuccess(QBDialog dialog, Bundle args) {

                    startGroupChat(dialog);

                }

                @Override
                public void onError(List<String> errors) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setMessage("dialog creation errors: " + errors).create().show();
                }
            });
            //   Toast.makeText(getApplicationContext(), "Select only one to chat", Toast.LENGTH_SHORT).show();
        }
    }

    public void CreateAdListingAsPrivateChat(String dialogName, int occupantId) {

        int a1[] = { occupantId};
        ArrayList<Integer> arl = new ArrayList<Integer>();
        for (int i : a1) {
            arl.add(i);
            Map<String, String> data = new HashMap<String, String>();
            data.put("data[class_name]", "zoneinfo");
            data.put("data[zoneid]", Const.zoneId);


            QBDialog dialogToCreate = new QBDialog();
            dialogToCreate.setName(dialogName);
            dialogToCreate.setData(data);
            //ChatService.getInstance().addDialogsUsers();
            dialogToCreate.setType(QBDialogType.PRIVATE);
            dialogToCreate.setOccupantsIds(arl);
            dialogToCreate.setName(dialogName);

            QBChatService.getInstance().getGroupChatManager().createDialog(dialogToCreate, new QBEntityCallbackImpl<QBDialog>() {
                @Override
                public void onSuccess(QBDialog dialog, Bundle args) {

                    startPrivateChat(dialog);

                }

                @Override
                public void onError(List<String> errors) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setMessage("dialog creation errors: " + errors).create().show();
                }
            });
            //   Toast.makeText(getApplicationContext(), "Select only one to chat", Toast.LENGTH_SHORT).show();
        }
    }

    private void startGroupChat(QBDialog dialog){
        Bundle bundle = new Bundle();
        bundle.putSerializable(ChatActivity.EXTRA_DIALOG, dialog);

        ChatActivity.start(context, bundle);
    }

    private void startPrivateChat(QBDialog dialog){
        Bundle bundle = new Bundle();
        bundle.putSerializable(ChatActivity.EXTRA_DIALOG, dialog);

        ChatActivity.start(context, bundle);
    }
    public void submitAdd() {
        // TODO Auto-generated method stub
        boolean wrapInScrollView = true;
        MaterialDialog submitFeedbackDialog = new MaterialDialog.Builder(context)
                .theme(Theme.LIGHT)
                .title("Post Your Ad")
                .positiveText("Submit")
                .negativeText("Cancel")
                .negativeColorRes(R.color.button_blue_normal_color)
                .positiveColorRes(R.color.button_blue_pressed_color)
                .customView(R.layout.change_password_dialog, wrapInScrollView)
                .callback(new MaterialDialog.ButtonCallback() {


                    @Override
                    public void onPositive(MaterialDialog materialDialog) {


                        String postAdds = postAdd.getText().toString();



                        if ((postAdds.length() <= 8)) {

                            Toast.makeText(context, "Please enter more than 10 characters", Toast.LENGTH_LONG).show();
                        } else {
                            //Toast.makeText(getActivity(), "Password must be more than 5 characters long.", Toast.LENGTH_LONG).show();

                           /* prevmessage = messageView.getText().toString();
                            prevtitle = titleView.getText().toString();*/
                            CreateAdListingAsGroupChat(postAdd.getText().toString());

                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog materialDialog) {

                    }
                })
                .build();

        View submitView = submitFeedbackDialog.getCustomView();
        postAdd = (EditText) submitView
                .findViewById(R.id.postAdd);

        /**
         * Don't understand
         */
       /* messageView.setText(prevmessage);
        titleView.setText(prevtitle);*/

       /* serviceView = (CustomMaterialEditText) submitView.findViewById(R.id.category_view);
        serviceView.setFocusable(false);
        serviceView.setFocusableInTouchMode(false);
        serviceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServiceDialog();
            }
        });*/

        submitFeedbackDialog.show();
    }

    public void submitPrivateDialog(final int occupantId) {
        // TODO Auto-generated method stub
        boolean wrapInScrollView = true;
        MaterialDialog submitFeedbackDialog = new MaterialDialog.Builder(context)
                .theme(Theme.LIGHT)
                .title("Are you sure you want to chat with the User?")
                .positiveText("Yes")
                .negativeText("No")
                .negativeColorRes(R.color.button_blue_normal_color)
                .positiveColorRes(R.color.button_blue_pressed_color)
                .customView(R.layout.change_password_dialog, wrapInScrollView)
                .callback(new MaterialDialog.ButtonCallback() {


                    @Override
                    public void onPositive(MaterialDialog materialDialog) {



                            CreateAdListingAsPrivateChat("Private Chat", occupantId);


                    }

                    @Override
                    public void onNegative(MaterialDialog materialDialog) {

                    }
                })
                .build();

        View submitView = submitFeedbackDialog.getCustomView();
        postAdd = (EditText) submitView
                .findViewById(R.id.postAdd);

        /**
         * Don't understand
         */
       /* messageView.setText(prevmessage);
        titleView.setText(prevtitle);*/

       /* serviceView = (CustomMaterialEditText) submitView.findViewById(R.id.category_view);
        serviceView.setFocusable(false);
        serviceView.setFocusableInTouchMode(false);
        serviceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServiceDialog();
            }
        });*/

        submitFeedbackDialog.show();
    }
}
