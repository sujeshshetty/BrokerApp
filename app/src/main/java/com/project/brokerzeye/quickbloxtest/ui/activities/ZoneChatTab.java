package com.project.brokerzeye.quickbloxtest.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.brokerzeye.R;

/**
 * Created by Edwin on 15/02/2015.
 */
public class ZoneChatTab extends Fragment {
    
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.zone_chat_tab,container,false);
        return v;
    }
}