package com.project.brokerzeye.request;

/**
 * Created by shetty on 10/1/2015.
 */
public class LoginResponse {


    /**
     * user : {"userId":149,"name":"Sujesh","phoneNumber":"05612345678","emailId":"sujeshshetty64@gmail.com","profImgLocation":"http://122.166.212.177:7777/images/Sujesh05612345678/profileImage/image.jpg","appkey":"D95527E7AB43E938B3B8FE78CEC407DB","regId":"APA91bGuAVoFfZmtjTjcBXMjD8R0FlRtIY7GPqLyAh-678QUBTdafsuiLp7FrfQxBN8x7Qlq4At345TiRW8wJlf9q8zTT5noQXiTaez6992s-kc66TH572Q","creationTime":"Oct 18, 2015 9:02:54 PM","updatedTime":"Oct 19, 2015 5:59:59 PM","validated":true,"statusEnum":"ONLINE","zoneCount":3,"adsCreated":0,"adsToRespond":0}
     * status : {"code":200,"message":"Successfully LoggedIn! ","errorMessage":""}
     */

    private UserEntity user;
    private StatusEntity status;

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public UserEntity getUser() {
        return user;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public static class UserEntity {
        /**
         * userId : 149
         * name : Sujesh
         * phoneNumber : 05612345678
         * emailId : sujeshshetty64@gmail.com
         * profImgLocation : http://122.166.212.177:7777/images/Sujesh05612345678/profileImage/image.jpg
         * appkey : D95527E7AB43E938B3B8FE78CEC407DB
         * regId : APA91bGuAVoFfZmtjTjcBXMjD8R0FlRtIY7GPqLyAh-678QUBTdafsuiLp7FrfQxBN8x7Qlq4At345TiRW8wJlf9q8zTT5noQXiTaez6992s-kc66TH572Q
         * creationTime : Oct 18, 2015 9:02:54 PM
         * updatedTime : Oct 19, 2015 5:59:59 PM
         * validated : true
         * statusEnum : ONLINE
         * zoneCount : 3
         * adsCreated : 0
         * adsToRespond : 0
         */

        private int userId;
        private String name;
        private String phoneNumber;
        private String emailId;
        private String profImgLocation;
        private String appkey;
        private String regId;
        private String creationTime;
        private String updatedTime;
        private boolean validated;
        private String statusEnum;
        private int zoneCount;
        private int adsCreated;
        private int adsToRespond;

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public void setProfImgLocation(String profImgLocation) {
            this.profImgLocation = profImgLocation;
        }

        public void setAppkey(String appkey) {
            this.appkey = appkey;
        }

        public void setRegId(String regId) {
            this.regId = regId;
        }

        public void setCreationTime(String creationTime) {
            this.creationTime = creationTime;
        }

        public void setUpdatedTime(String updatedTime) {
            this.updatedTime = updatedTime;
        }

        public void setValidated(boolean validated) {
            this.validated = validated;
        }

        public void setStatusEnum(String statusEnum) {
            this.statusEnum = statusEnum;
        }

        public void setZoneCount(int zoneCount) {
            this.zoneCount = zoneCount;
        }

        public void setAdsCreated(int adsCreated) {
            this.adsCreated = adsCreated;
        }

        public void setAdsToRespond(int adsToRespond) {
            this.adsToRespond = adsToRespond;
        }

        public int getUserId() {
            return userId;
        }

        public String getName() {
            return name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getEmailId() {
            return emailId;
        }

        public String getProfImgLocation() {
            return profImgLocation;
        }

        public String getAppkey() {
            return appkey;
        }

        public String getRegId() {
            return regId;
        }

        public String getCreationTime() {
            return creationTime;
        }

        public String getUpdatedTime() {
            return updatedTime;
        }

        public boolean getValidated() {
            return validated;
        }

        public String getStatusEnum() {
            return statusEnum;
        }

        public int getZoneCount() {
            return zoneCount;
        }

        public int getAdsCreated() {
            return adsCreated;
        }

        public int getAdsToRespond() {
            return adsToRespond;
        }
    }

    public static class StatusEntity {
        /**
         * code : 200
         * message : Successfully LoggedIn!
         * errorMessage :
         */

        private int code;
        private String message;
        private String errorMessage;

        public void setCode(int code) {
            this.code = code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }
}
