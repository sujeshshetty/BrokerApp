package com.project.brokerzeye;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.brokerzeye.api.PostApi;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.GenericAsynTask.TaskListener;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.request.LoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements TextWatcher {

	private EditText phonenumber_edt, password_edt;
	private String phonenmuber_str = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		getSupportActionBar().hide();
		
		String password_str = "<font color='#000000'>Password </font>" + "<font color='#FF0000'>*</font>";
		String phone_str = "<font color='#000000'>Mobile Number </font>" + "<font color='#FF0000'>*</font>";
		
		phonenumber_edt = (EditText)findViewById(R.id.phonenumber_edt);
		phonenumber_edt.setHint(Html.fromHtml(phone_str));
		phonenumber_edt.addTextChangedListener(this);
		password_edt = (EditText)findViewById(R.id.password_edt);
		password_edt.setHint(Html.fromHtml(password_str));
		password_edt.addTextChangedListener(this);

		Button signup_btn = (Button)findViewById(R.id.signup_btn);
		signup_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
			}
		});

		Button signin_btn = (Button)findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
                LoginApi();
            }
		});

		Button forgot_pass_btn = (Button)findViewById(R.id.forgot_pass_btn);
		forgot_pass_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showForgetPassDialog();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		phonenumber_edt.setError(null);
		password_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}

	private void showForgetPassDialog(){
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.forgot_pass_dialog);
		dialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		dialog.setTitle("Forgot Password?");
		final EditText phnnumber_edt = (EditText)dialog.findViewById(R.id.phnnumber_edt);
		final Spinner country_code_edt = (Spinner)dialog.findViewById(R.id.country_code_edt);
		Button submit_btn = (Button)dialog.findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Connection.checkConnection(LoginActivity.this)) {
                    String phonenmuber_str = phnnumber_edt.getText().toString().trim();
                    if (!Util.ValidatePhoneNumber(phonenmuber_str))
						phnnumber_edt.setError("please enter valid phone number");
                    else {
						phonenmuber_str = country_code_edt.getSelectedItem().toString()+phonenmuber_str;
                        String url = Util.FORGOT_PASSWORD_API + "?phoneNumber=" + phonenmuber_str;
                        GenericAsynTask task = new GenericAsynTask("Please wait...", LoginActivity.this,
                                url, null);
						final String finalPhonenmuber_str = phonenmuber_str;
						task.setOnTaskListener(new TaskListener() {

                            @Override
                            public void onSuccess(String success) {
                                // TODO Auto-generated method stub
                                Log.i("forgot pass---", success + "");
                                try {
                                    JSONObject res_obj = new JSONObject(success);
                                    String res_code = res_obj.getString("code");
                                    String message = res_obj.getString("message");
                                    if (res_code.equals("200")) {
                                        showEnterOTPDialog(finalPhonenmuber_str);
                                        dialog.dismiss();
                                    }
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(String error) {
                                // TODO Auto-generated method stub
                                Toast.makeText(LoginActivity.this, "Something went wrong. Please try again",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                        task.execute();
                    }
                } else
                    Toast.makeText(LoginActivity.this, "Please enable internet connection",
                            Toast.LENGTH_SHORT).show();
            }
        });
		dialog.show();
	}

    public void LoginApi() {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        final String phnnumber_str = phonenumber_edt.getText().toString().trim();
        final String password_str = password_edt.getText().toString().trim();
        final String regId = Util.getRegistrationId(LoginActivity.this);
        if (!Util.ValidateSigninPhoneNumber(phnnumber_str))
			phonenumber_edt.setError("please enter valid phone number");
        else if (password_str.equals(""))
            password_edt.setError("please enter password");
        else if (regId == null || regId.equals(""))
            showDialog("Error!", "Your device is not properly setup for this application");
        else {
            final HashMap<String, String> params = new HashMap<String, String>();

            params.put("phoneNumber", phnnumber_str);
            params.put("password", password_str);
            params.put("regId", regId);
			App.getPref().put("phoneNumber", phnnumber_str);
			App.getPref().put("password", password_str);
			App.getPref().put("regId", regId);
            //String hardcodedDomain= "http://172.16.1.94:8080/TelematicsRESTService/services/ServiceProcess/";
            String URL = "http://122.166.212.177:7777/dubaiapp/rest/loginservice/login";//+ Constants.UPLOAD_BILLS;
            Log.i("", "Login  url : " + URL);
            Log.e("", "Login Params: " + new JSONObject(params));
            PostApi api = new PostApi(Request.Method.POST, URL, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {

                    Log.e("", "Login API RESPONSE RCVD : " + jsonObject);
                    try {
						App.getPref().put("phoneNumber", phnnumber_str);
						App.getPref().put("password",password_str);
						App.getPref().put("regId",regId);

                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        LoginResponse getTripDetailsResponse = gson.fromJson(String.valueOf(jsonObject), LoginResponse.class);
                   /* JSONObject res_obj = new JSONObject(success);
                    JSONObject status_obj = res_obj.getJSONObject("status");
                    String res_code = status_obj.getString("code");
                    String message = status_obj.getString("message");*/
                        Toast.makeText(LoginActivity.this, getTripDetailsResponse.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        if (getTripDetailsResponse.getStatus().getCode() == 200) {
                            //  JSONObject user_obj = res_obj.getJSONObject("user");
							App.getPref().put("mobileNumber",phnnumber_str);

                            DBQueryHelper dbq = new DBQueryHelper(LoginActivity.this);
                            dbq.open();
                            dbq.insertLogin(getTripDetailsResponse.getUser().getPhoneNumber());
                            dbq.insertProfile(String.valueOf(getTripDetailsResponse.getUser().getUserId()), getTripDetailsResponse.getUser().getName(),
                                    getTripDetailsResponse.getUser().getPhoneNumber(), getTripDetailsResponse.getUser().getEmailId(),
                                    getTripDetailsResponse.getUser().getAppkey());
                            dbq.close();
                            Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);

                            intent.putExtra("LoginResponse",String.valueOf(jsonObject));
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Log.e("", "Volley Error Login: " + volleyError);

                }
            });

            App.getVolleyQueue().add(api);
        }
    }

	private void showEnterOTPDialog(final String mobileNumber){
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.enter_otp_dialog);
		dialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		dialog.setTitle("Enter OTP");
		final EditText otp_edt = (EditText)dialog.findViewById(R.id.otp_edt);
		Button submit_btn = (Button)dialog.findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(LoginActivity.this)){
					String otp_str = otp_edt.getText().toString().trim();
					if(otp_str.equals(""))
						otp_edt.setError("please enter OTP");
					else{
						String url = Util.VALIDATE_OTP_API+"?phoneNumber="+mobileNumber+"&otp="+otp_str;
						GenericAsynTask task = new GenericAsynTask("Please wait...", LoginActivity.this,
								url, null);
						task.setOnTaskListener(new TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("verification---", success+"");
								try {
									JSONObject obj = new JSONObject(success);
									JSONObject status_obj = obj.getJSONObject("status");
									String res_code = status_obj.getString("code");
									String message = status_obj.getString("message");
									Toast.makeText(LoginActivity.this, message,
											Toast.LENGTH_SHORT).show();
									if(res_code.equals("200")){
										JSONObject usr_obj = obj.getJSONObject("user");
										String appkey = usr_obj.getString("appkey");
										Intent intent = new Intent(LoginActivity.this,
												ChangePasswordActivity.class);
										intent.putExtra("appkey", appkey);
										intent.putExtra("phonenmuber", phonenmuber_str);
										startActivity(intent);
										dialog.dismiss();
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(LoginActivity.this, "Something went wrong. Please try again",
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(LoginActivity.this, "Please enable internet connection",
							Toast.LENGTH_SHORT).show();
			}
		});
		dialog.show();
	}

	//Ram's code
	// TODO Auto-generated method stub
	/*if(Connection.checkConnection(LoginActivity.this)){
		final String phnnumber_str = phonenumber_edt.getText().toString().trim();
		String password_str = password_edt.getText().toString().trim();
		String regId = Util.getRegistrationId(LoginActivity.this);
		if(!Util.ValidatePhoneNumber(phnnumber_str))
			phonenumber_edt.setError("please enter valid phone number");
		else if(password_str.equals(""))
			password_edt.setError("please enter password");
		else if(regId==null || regId.equals(""))
			showDialog("Error!", "Your device is not properly setup for this application");
		else{
			JSONObject obj = new JSONObject();
			try {
				obj.put("phoneNumber", phnnumber_str);
				obj.put("password", password_str);
				obj.put("regId", regId);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			GenericAsynTask task = new GenericAsynTask("Authenticating...", LoginActivity.this,
					Util.LOGIN_API, obj);
			task.setOnTaskListener(new GenericAsynTask.TaskListener() {

				@Override
				public void onSuccess(String success) {
					// TODO Auto-generated method stub
					Log.i("login---", success+"");
					try {
						JSONObject res_obj = new JSONObject(success);
						JSONObject status_obj = res_obj.getJSONObject("status");
						String res_code = status_obj.getString("code");
						String message = status_obj.getString("message");
						Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
						if(res_code.equals("200")){
							JSONObject user_obj = res_obj.getJSONObject("user");
							DBQueryHelper dbq = new DBQueryHelper(LoginActivity.this);
							dbq.open();
							dbq.insertLogin(phnnumber_str);
							dbq.insertProfile(user_obj.getString("Id"), user_obj.getString("name"),
									user_obj.getString("phoneNumber"), user_obj.getString("emailId"),
									user_obj.getString("appkey"));
							dbq.close();
							Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
							startActivity(intent);
							finish();
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(String error) {
					// TODO Auto-generated method stub
					Toast.makeText(LoginActivity.this, "Something went wrong. Please try again",
							Toast.LENGTH_SHORT).show();
				}
			});
			task.execute();
		}
	}
	else
			Toast.makeText(LoginActivity.this, "Please enable internet connection",
	Toast.LENGTH_SHORT).show();*/
}
