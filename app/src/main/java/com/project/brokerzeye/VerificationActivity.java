package com.project.brokerzeye;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class VerificationActivity extends AppCompatActivity implements TextWatcher{

	private EditText code_edt;
	String mobileNumber;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verification);
		Bundle bundle = getIntent().getExtras();


		mobileNumber = bundle.getString("mobileNumber");
		getSupportActionBar().hide();
		code_edt = (EditText)findViewById(R.id.code_edt);
		code_edt.addTextChangedListener(this);

		Button signin_btn = (Button)findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		RelativeLayout back_btn = (RelativeLayout)findViewById(R.id.back_btn);
		back_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Button verify_btn = (Button)findViewById(R.id.verify_btn);
		verify_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(VerificationActivity.this)){
					String code_str = code_edt.getText().toString().trim();
					if(code_str.equals(""))
						code_edt.setError("please enter OTP");
					else{
						String url = Util.VALIDATE_OTP_API+"?otp="+code_str+"&phoneNumber="+mobileNumber;
						GenericAsynTask task = new GenericAsynTask("Validating...",
								VerificationActivity.this, url, null);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("verification---", success+"");
								try {
									JSONObject obj = new JSONObject(success);
									JSONObject status_obj = obj.getJSONObject("status");
									String res_code = status_obj.getString("code");
									String message = status_obj.getString("message");
									Toast.makeText(VerificationActivity.this, message,
											Toast.LENGTH_SHORT).show();
									if(res_code.equals("200"))
										finish();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(VerificationActivity.this, "Something went wrong. Please try again",
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(VerificationActivity.this, "Please enable internet connection",
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		code_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
								  int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

}
