package com.project.brokerzeye;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.project.brokerzeye.adapter.ProfessionAdapter;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.model.ProfessionModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SignUpActivity extends AppCompatActivity implements TextWatcher {

	private EditText name_edt, email_edt, password_edt, phone_edt;
	private EditText profession_edt, subprofession_edt;
	private String profession_id = "";
	private boolean bank = false;
	private Spinner country_code_edt;
	private EditText reference_no_edt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		String name_str = "<font color='#000000'>Name </font>" + "<font color='#FF0000'>*</font>";
		String email_str = "<font color='#000000'>Email </font>" + "<font color='#FF0000'>*</font>";
		String password_str = "<font color='#000000'>Password </font>" + "<font color='#FF0000'>*</font>";
		String phone_str = "<font color='#000000'>Mobile Number </font>" + "<font color='#FF0000'>*</font>";
		String profession_str = "<font color='#000000'>Select Profession </font>" + "<font color='#FF0000'>*</font>";
		String subprofession_str = "<font color='#000000'>Enter Bank Name </font>" + "<font color='#FF0000'>*</font>";

		getSupportActionBar().hide();
		name_edt = (EditText)findViewById(R.id.name_edt);
		name_edt.setHint(Html.fromHtml(name_str));
		name_edt.addTextChangedListener(this);
		reference_no_edt= (EditText)findViewById(R.id.reference_no_edt);
		email_edt = (EditText)findViewById(R.id.email_edt);
		email_edt.setHint(Html.fromHtml(email_str));
		email_edt.addTextChangedListener(this);
		password_edt = (EditText)findViewById(R.id.password_edt);
		password_edt.setHint(Html.fromHtml(password_str));
		password_edt.addTextChangedListener(this);
		phone_edt = (EditText)findViewById(R.id.phone_edt);
		phone_edt.setHint(Html.fromHtml(phone_str));
		phone_edt.addTextChangedListener(this);
		profession_edt = (EditText)findViewById(R.id.profession_edt);
		profession_edt.setHint(Html.fromHtml(profession_str));
		profession_edt.addTextChangedListener(this);
		subprofession_edt = (EditText)findViewById(R.id.subprofession_edt);
		subprofession_edt.setHint(Html.fromHtml(subprofession_str));
		subprofession_edt.addTextChangedListener(this);
		country_code_edt = (Spinner)findViewById(R.id.country_code_edt);

		profession_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus)
					showProfessionDialog("Select Profession", profession_edt,
							getProfessionList());
			}
		});

		profession_edt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showProfessionDialog("Select Profession", profession_edt,
						getProfessionList());
			}
		});

		Button signin_btn = (Button)findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		RelativeLayout back_btn = (RelativeLayout)findViewById(R.id.back_btn);
		back_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Button join_btn = (Button)findViewById(R.id.join_btn);
		join_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(SignUpActivity.this)){
					String name_str = name_edt.getText().toString().trim();
					String email_str = email_edt.getText().toString().trim();
					String password_str = password_edt.getText().toString().trim();
					String phone_str = phone_edt.getText().toString().trim();
					String regId = Util.getRegistrationId(SignUpActivity.this);
					String bank_name = subprofession_edt.getText().toString().trim();
					String reference_number=reference_no_edt.getText().toString().trim();
					if(!reference_number.matches("[a-zA-Z ]*"))
						reference_no_edt.setError("please enter valid reference number");
					else if(name_str.equals("") || !name_str.matches("[a-zA-Z ]*"))
						name_edt.setError("please enter valid name");
					else if(profession_id.equals(""))
						profession_edt.setError("please select profession");
					else if(bank && bank_name.equals(""))
						subprofession_edt.setError("please enter bank name");
					else if(!Util.isEmailValid(email_str))
						email_edt.setError("please enter valid email");
					else if(!Util.PASSWORD_PATTERN.matcher(password_str).matches())
						password_edt.setError("enter password with min 8 characters and capital, numerical and special characters");
					else if(!Util.ValidatePhoneNumber(phone_str))
						phone_edt.setError("please enter valid phone number");
					else if(regId==null || regId.equals(""))
						showDialog("Error!", "Your device is not properly setup for this application");
					else{
						phone_str = country_code_edt.getSelectedItem().toString()+phone_str;
						JSONObject obj = new JSONObject();
						try {
							obj.put("name", name_str);
							obj.put("password", password_str);
							obj.put("phoneNumber", phone_str);
							obj.put("emailId", email_str);
							obj.put("professionId", profession_id);
							obj.put("bankName", bank_name);
							obj.put("regId", regId);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("Please wait...", SignUpActivity.this,
								Util.REGISTRATION_API, obj);
						final String finalPhone_str = phone_str;
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("registration---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									if(res_obj.has("status")){
										JSONObject status_obj = res_obj.getJSONObject("status");
										String res_code = status_obj.getString("code");
										String message = status_obj.getString("message");
										if(res_code.equals("200")){
											Toast.makeText(SignUpActivity.this, "Verification sent on registered email. " +
													"Kindly insert the verification code.", Toast.LENGTH_LONG).show();
											startActivity(new Intent(SignUpActivity.this,
													VerificationActivity.class).putExtra("mobileNumber", finalPhone_str));
											finish();
										}
										else
											Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
									}
									else{
										String message = res_obj.getString("message");
										Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(SignUpActivity.this, "Something went wrong. Please try again",
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(SignUpActivity.this, "Please enable internet connection",
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		name_edt.setError(null);
		profession_edt.setError(null);
		email_edt.setError(null);
		password_edt.setError(null);
		phone_edt.setError(null);
		subprofession_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
								  int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}

	private ArrayList<ProfessionModel> getProfessionList(){
		DBQueryHelper dbq = new DBQueryHelper(this);
		dbq.open();
		ArrayList<ProfessionModel> pro_list = dbq.getProfession();
		dbq.close();
		return pro_list;
	}

	private void showProfessionDialog(String title, final EditText edt,
			final ArrayList<ProfessionModel> pro_list){
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.select_profession_dialog);
		dialog.setTitle(title);
		ListView profession_list = (ListView)dialog.findViewById(R.id.profession_list);
		final ProfessionAdapter adapter = new ProfessionAdapter(this);
		adapter.clear();
		adapter.addAll(pro_list);
		profession_list.setAdapter(adapter);
		Button submit_btn = (Button)dialog.findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				profession_id = "";
				String pro_name = "";
				for(ProfessionModel model : pro_list){
					if(model.isSelected()){
						if(profession_id.equals(""))
							profession_id = model.getId();
						else
							profession_id = profession_id+","+model.getId();
						if(pro_name.equals(""))
							pro_name = model.getName();
						else
							pro_name = pro_name+","+model.getName();
					}
				}
				if(pro_name.contains("Banker")){
					subprofession_edt.setVisibility(View.VISIBLE);
					bank = true;
				}
				else{
					subprofession_edt.setVisibility(View.GONE);
					bank = false;
				}
				profession_edt.setText(pro_name);
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
