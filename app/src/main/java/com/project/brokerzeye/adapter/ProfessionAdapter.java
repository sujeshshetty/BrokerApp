package com.project.brokerzeye.adapter;

import java.util.ArrayList;
import java.util.Collection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.project.brokerzeye.R;
import com.project.brokerzeye.model.ProfessionModel;

public class ProfessionAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private ArrayList<ProfessionModel> pro_list = new ArrayList<ProfessionModel>();

	public ProfessionAdapter(Context context){
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public boolean addAll(Collection<? extends ProfessionModel> collection) {
		return pro_list.addAll(collection);
	}

	public void clear() {
		pro_list.clear();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pro_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return pro_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView==null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.profession_adapter, parent, false);
			holder.pro_name = (CheckBox)convertView.findViewById(R.id.pro_name);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();

		holder.pro_name.setText(pro_list.get(position).getName());
		
		holder.pro_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					pro_list.get(position).setSelected(true);
				else
					pro_list.get(position).setSelected(false);
			}
		});

		return convertView;
	}

	private class ViewHolder{
		CheckBox pro_name;
	}
}
