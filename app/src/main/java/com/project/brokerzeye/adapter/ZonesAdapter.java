package com.project.brokerzeye.adapter;

import java.util.ArrayList;
import java.util.Collection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.project.brokerzeye.R;
import com.project.brokerzeye.common.Util.AnimateFirstDisplayListener;
import com.project.brokerzeye.model.ZonesModel;

public class ZonesAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private DisplayImageOptions options;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public ZonesAdapter(Context context) {
		// TODO Auto-generated constructor stub
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(!imageLoader.isInited())
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		options = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		.showImageOnLoading(R.drawable.ic_top_image_60)
		.showImageOnFail(R.drawable.ic_top_image_60)
		.build();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return zone_list.size();
	}

	public boolean addAll(Collection<? extends ZonesModel> collection) {
		return zone_list.addAll(collection);
	}

	public void clear() {
		zone_list.clear();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView==null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.adapter_zones, parent, false);
			holder.background_img = (ImageView)convertView.findViewById(R.id.background_img);
			holder.zone_name_txt = (TextView)convertView.findViewById(R.id.zone_name_txt);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();

		holder.zone_name_txt.setText(zone_list.get(position).getName());
		imageLoader.displayImage(zone_list.get(position).getImage(), 
				holder.background_img, options, animateFirstListener);

		return convertView;
	}

	private class ViewHolder{
		ImageView background_img;
		TextView zone_name_txt;
	}
}
