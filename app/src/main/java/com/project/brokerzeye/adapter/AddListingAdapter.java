package com.project.brokerzeye.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.project.brokerzeye.R;

@SuppressLint("ViewHolder")
public class AddListingAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private ArrayList<String> add_list;
	TextView add_txt;
	FragmentActivity fm;

	public AddListingAdapter(FragmentActivity fragmentActivity, ArrayList<String> ad_array) {
		add_list=ad_array;
		fm=fragmentActivity;
		System.out.println("ffffffffffff"+add_list.get(0).toString());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return add_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return add_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (inflater == null)
			inflater = (LayoutInflater) fm
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
		convertView = inflater.inflate(R.layout.ad_listing_item, null);
		
		
		add_txt = (TextView)convertView.findViewById(R.id.textView_ad);
		add_txt.setText(add_list.get(position).toString());
		return convertView;
	}
	

}
