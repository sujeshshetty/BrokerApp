package com.project.brokerzeye.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.brokerzeye.R;

public class MenuAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private String[] menu_arr;

	public MenuAdapter(Context context) {
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		menu_arr = context.getResources().getStringArray(R.array.menu_item);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return menu_arr.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView==null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.menu_adapter, parent, false);
			holder.menu_item_txt = (TextView)convertView.findViewById(R.id.menu_item_txt);
			holder.red = (ImageView)convertView.findViewById(R.id.red);
			holder.green = (ImageView)convertView.findViewById(R.id.green);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder)convertView.getTag();

		holder.menu_item_txt.setText(menu_arr[position]);
		holder.green.setVisibility(View.GONE);
		holder.red.setVisibility(View.GONE);
		if(menu_arr[position].equals("Zones"))
		{
		//	holder.green.setVisibility(View.VISIBLE);
		}
		else
		{
		//	holder.green.setVisibility(View.GONE);
		}

		if(menu_arr[position].equals("Ads Placed"))
		{
		//	holder.red.setVisibility(View.VISIBLE);
		}
		else
		{
			//holder.red.setVisibility(View.GONE);
		}


		return convertView;
	}

	private class ViewHolder {
		TextView menu_item_txt;
		ImageView red,green;
	}
}
