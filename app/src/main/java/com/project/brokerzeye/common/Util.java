package com.project.brokerzeye.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.project.brokerzeye.SplashActivity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

	/**
	 * API
	 */
	public static final String SERVER_URL = "http://122.166.212.177:7777/dubaiapp/rest/";
	public static final String LOGIN_API = SERVER_URL+"loginservice/login";
	public static final String REGISTRATION_API = SERVER_URL+"loginservice/register";
	public static final String FORGOT_PASSWORD_API = SERVER_URL+"loginservice/forgotPwd";
	public static final String UPDATE_GCM_API = SERVER_URL+"loginservice/updateGCMId";
	public static final String CHANGE_PASSWORD_API = SERVER_URL+"loginservice/changePwd";
	public static final String USER_ZONES_API = SERVER_URL+"zoneservice/userZones";
	public static final String NEW_ZONE_API = SERVER_URL+"zoneservice/requestNewZones";
	public static final String VALIDATE_OTP_API = SERVER_URL+"loginservice/validateotp";
	public static final String UPDATE_PROFILE_API = "http://122.166.212.177:7777/dubaiapp/profImageUpload";
	public static final String SAVE_PROFILE_API ="http://122.166.212.177:7777/dubaiapp/rest/userservice/saveProfile";

	public static final String ALL_PROFESSION_API = SERVER_URL+"professionservice/getAllProfessions";
	public static final String SAVE_ZONES_API = SERVER_URL+"zoneservice/saveUserZones";
	public static final String DELETE_ZONES_API = SERVER_URL+"zoneservice/deleteUserZones";


	/**
	 * GCM
	 */
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String SENDER_ID = "556696427388";

	public static final Pattern PASSWORD_PATTERN = Pattern
			.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%,.]*).{8,20}");

	@SuppressLint("NewApi")
	public static String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("Demo", "Registration not found.");
			return "";
		}

		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			return "";
		}
		return registrationId;
	}

	public static SharedPreferences getGCMPreferences(Context context) {

		return context.getSharedPreferences(SplashActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Shared Preference
	 */

	public static void writePreference(Context mctx, String key, String value)
	{
		SharedPreferences prefs = mctx.getSharedPreferences("demo_preference", 0);
		SharedPreferences.Editor editor= prefs.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String readPreference(Context mctx, String key)
	{
		SharedPreferences prefs = mctx.getSharedPreferences("demo_preference", 0);
		String value= prefs.getString(key, "");
		return value;
	}


	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}

		return isValid;
	}

	/*public static boolean ValidatePhoneNumber(String phone) {
		String[] format={"\\d{3}-\\d{3}-\\d{4}","\\(\\d{3}\\)\\ \\d{3}-\\d{4}","\\d{3}\\ \\d{3}\\ \\d{4}","\\d{3}.\\d{3}.\\d{4}","\\d{10}"};

		for(int s=0;s<format.length;s++)
		{
			Pattern pattern = Pattern.compile(format[s]);
			Matcher matcher = pattern.matcher(phone);
			if (matcher.matches())
				return true;
		}
		return false;
	}*/
	
	public static boolean ValidatePhoneNumber(String phone) {
		if(phone.length()==8 || phone.length()==7)
		{
			if (phone.matches("[0-9]+"))
				return true;
				else
				return false;


			}
		else {
			return false;
		}

	}
	public static boolean ValidateSigninPhoneNumber(String phone) {
		if(phone.length()==11)
			return true;
		return false;
	}
	public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		public static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	public static boolean isAlphaNumeric(String value) {
		Pattern p = Pattern.compile("[^a-zA-Z0-9]");
		boolean hasSpecialChar = p.matcher(value).find();
		return hasSpecialChar;
	}

}
