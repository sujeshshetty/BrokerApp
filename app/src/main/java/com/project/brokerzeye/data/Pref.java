package com.project.brokerzeye.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.project.brokerzeye.constants.Constants;


/**
 * Created by vijay on 28/5/15.
 */
public class Pref {

    private Context mContext;

    public Pref(Context context) {

        this.mContext = context;
    }

    public String getString(String key) {

        SharedPreferences settings = mContext.getSharedPreferences(
                Constants.SHARED_PREF, 0);
        try {
            String value = settings.getString(key, null);
            return value;
        } catch (Exception e) {
            return null;
        }
    }
    public int getInt(String key) {

        SharedPreferences settings = mContext.getSharedPreferences(
                Constants.SHARED_PREF, 0);
        try {
            int value = settings.getInt(key, 0);
            return value;
        } catch (Exception e) {
            return 0;
        }
    }
    public void put(String key, String value) {
        SharedPreferences settings = mContext.getSharedPreferences(
                Constants.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void put(String key, int value) {
        SharedPreferences settings = mContext.getSharedPreferences(
                Constants.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    public void remove(String key) {
        SharedPreferences settings = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        settings.edit().remove(key).commit();
    }
}
