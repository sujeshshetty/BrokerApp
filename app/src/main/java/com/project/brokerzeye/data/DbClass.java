package com.project.brokerzeye.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DbClass extends SQLiteOpenHelper {


    private static final int    DB_VERSION  =  1;
    private static final String DB_NAME     =  "brokerapp.db";
    private SQLiteDatabase db;


    public static final String TABLE_USER_PROFILE = "user_profile";
    public static final String TABLE_LOGIN = "login";
    public static final String TABLE_ZONE = "zone";
    public static final String TABLE_PROFESSION = "profession";

    public static final String COL_PRIMARY_KEY = "_id";

    /************************************/
    /*
     * RELATION TABLE COLUMNS
     */

    public static final String PROFILE_USERID     =  "userid";
    public static final String PROFILE_NAME     =  "name";
    public static final String PROFILE_PHONE     =  "phone";
    public static final String PROFILE_EMAIL     =  "email";
    public static final String PROFILE_APPKEY     =  "appkey";




    /*
        * CREATE TABLE QUERYS
        */
    private static final String CREATE_TABLE_PROFILE= "CREATE TABLE "
            + TABLE_USER_PROFILE + " ("
            + COL_PRIMARY_KEY + " INTEGER PRIMARY KEY, "
            + PROFILE_USERID + " text, "
            + PROFILE_NAME + " text, "
            + PROFILE_PHONE + " text, "
            + PROFILE_EMAIL + " text, "
            + PROFILE_APPKEY + " text);";

    /*private static final String CREATE_TABLE_ZONE= "CREATE TABLE "
            + TABLE_ZONE + " ("
            + COL_PRIMARY_KEY + " INTEGER PRIMARY KEY, "
            + PROFILE_USERID + " text, "
            + PROFILE_NAME + " text, "
            + PROFILE_PHONE + " text, "
            + PROFILE_EMAIL + " text, "
            + PROFILE_APPKEY + " text);";*/
  /*  private static final String CREATE_TABLE_LOGIN= "CREATE TABLE "
            + TABLE_LOGIN + " ("
            + COL_PRIMARY_KEY + " INTEGER PRIMARY KEY, "
            + PROFILE_USERID + " text, "
            + PROFILE_NAME + " text, "
            + PROFILE_PHONE + " text, "
            + PROFILE_EMAIL + " text, "
            + PROFILE_APPKEY + " text);";*/

    public DbClass(Context context) {
        super(context,DB_NAME, null, DB_VERSION);
    }

    public void initialize() {
        db = this.getWritableDatabase();
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL(CREATE_TABLE_PROFILE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_PROFILE);
    }
    public void insertUserDetails(ContentValues cons, int flag){
        try{
            db.beginTransaction();
            db.insertWithOnConflict(TABLE_USER_PROFILE, null, cons, flag);
            db.setTransactionSuccessful();
            db.endTransaction();
        }
        catch( Exception e ){
            Log.e("", "exception inserting user : " + e);
        }
    }


}
