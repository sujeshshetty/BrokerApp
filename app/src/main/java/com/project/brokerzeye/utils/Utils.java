package com.project.brokerzeye.utils;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vijay on 28/5/15.
 */
public class Utils {

    static final int MAX_WIDTH = 800;
    static final int MAX_HEIGHT = 800;
    public static String loadJSONApi(String file_name, Object class_object)
    {
        String file_path = "assets/" + file_name;
        String json = null;
        try
        {
            String file = file_path;
            InputStream is = class_object.getClass().getClassLoader().getResourceAsStream(file);
            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public static Bitmap resizeBitmap(String filePath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = 1;
        if ((MAX_WIDTH > 0) || (MAX_HEIGHT > 0)) {
            scaleFactor = Math.min(photoW / MAX_WIDTH, photoH / MAX_HEIGHT);
        }

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(filePath, bmOptions);
    }
    public static String getDisplayTime(String mysqlDateTime) {
        try {
            SimpleDateFormat curFormater = new SimpleDateFormat(
                    "yyyy-MM-dd k:m:s", Locale.UK);
            curFormater.setTimeZone(TimeZone.getDefault());
            Date dateObj = curFormater.parse(mysqlDateTime);
            SimpleDateFormat postFormater = new SimpleDateFormat(
                    "dd MMM h:mm a", Locale.UK);
            postFormater.setTimeZone(TimeZone.getDefault());
            String newDateStr = postFormater.format(dateObj);
            return newDateStr;
        } catch (Exception e) {
            return "";
        }
    }
public static String camelCase(String input) {

    return Character.toUpperCase(input.charAt(0)) + input.substring(1);
}

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public static void showDialog(final Context con,String title,String msg,boolean block) {
        AlertDialog.Builder b=new AlertDialog.Builder(con);
        if(title!=null) {
            b.setTitle(title);
        }
        if(msg!=null) {
            b.setMessage(msg);
        }
        if(block) {
            b.setCancelable(false);
            b.setPositiveButton("Update",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    //Try Google play
                    intent.setData(Uri.parse("market://details?id=com.weecode.portea"));
                    if (!startActivity(intent,con)) {
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?com.weecode.portea"));
                        if (!startActivity(intent,con)) {
                           Toast.makeText(con, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
        else {
            b.setPositiveButton("Update",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    //Try Google play
                    intent.setData(Uri.parse("market://details?id=com.weecode.portea"));
                    if (!startActivity(intent,con)) {
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?com.weecode.portea"));
                        if (!startActivity(intent,con)) {
                            Toast.makeText(con, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            b.setNegativeButton("Dismiss", null);
        }
        b.create().show();
    }
    public static boolean startActivity(Intent aIntent,Context con) {
        try
        {
            con.startActivity(aIntent);
            return true;
        }
        catch (ActivityNotFoundException e)
        {
            return false;
        }
    }
}
