package com.project.brokerzeye.model;

/**
 * Created by Shetty on 10/16/2015.
 */
public class QuickbloxSample {


    /**
     * session : {"_id":"5620e469a28f9a997a000f07","application_id":29576,"created_at":"2015-10-16T11:50:01Z","device_id":0,"nonce":305334,"token":"254172f81045107c14351384cfb2120bac728379","ts":1444996185,"updated_at":"2015-10-16T11:50:01Z","user_id":0,"id":48510}
     */

    private SessionEntity session;

    public void setSession(SessionEntity session) {
        this.session = session;
    }

    public SessionEntity getSession() {
        return session;
    }

    public static class SessionEntity {
        /**
         * _id : 5620e469a28f9a997a000f07
         * application_id : 29576
         * created_at : 2015-10-16T11:50:01Z
         * device_id : 0
         * nonce : 305334
         * token : 254172f81045107c14351384cfb2120bac728379
         * ts : 1444996185
         * updated_at : 2015-10-16T11:50:01Z
         * user_id : 0
         * id : 48510
         */

        private String _id;
        private int application_id;
        private String created_at;
        private int device_id;
        private int nonce;
        private String token;
        private int ts;
        private String updated_at;
        private int user_id;
        private int id;

        public void set_id(String _id) {
            this._id = _id;
        }

        public void setApplication_id(int application_id) {
            this.application_id = application_id;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setDevice_id(int device_id) {
            this.device_id = device_id;
        }

        public void setNonce(int nonce) {
            this.nonce = nonce;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public void setTs(int ts) {
            this.ts = ts;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String get_id() {
            return _id;
        }

        public int getApplication_id() {
            return application_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public int getDevice_id() {
            return device_id;
        }

        public int getNonce() {
            return nonce;
        }

        public String getToken() {
            return token;
        }

        public int getTs() {
            return ts;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public int getUser_id() {
            return user_id;
        }

        public int getId() {
            return id;
        }
    }
}
