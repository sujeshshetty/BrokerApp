package com.project.brokerzeye.api;

/**
 * Created by shetty on 10/2/2015.
 */

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.entity.mime.MultipartEntity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class MultipartImageRequest extends Request<String> {

    @SuppressWarnings("deprecation")
    private MultipartEntity entity = new MultipartEntity();


    private final Response.Listener<String> mListener;
    private final HashMap<String, String> params;

    public MultipartImageRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener, HashMap<String, String> params)
    {
        super(Method.POST, url, errorListener);

        mListener = listener;
        this.params = params;
        buildMultipartEntity();
    }

    @SuppressWarnings("deprecation")
    private void buildMultipartEntity()
    {

        for ( String key : params.keySet() ) {
           // entity.addPart(key, new StringBody(params.get(key), ContentType.TEXT_PLAIN));
        }


    }

    @SuppressWarnings("deprecation")
    @Override
    public String getBodyContentType()
    {
        return entity.getContentType().getValue();
    }

    @SuppressWarnings("deprecation")
    @Override
    public byte[] getBody() throws AuthFailureError
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try
        {
            entity.writeTo(bos);
        }
        catch (IOException e)
        {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    /**
     * copied from Android StringRequest class
     */
    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed = null;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

          parsed=new String(response.statusCode+"`"+response.headers.get("IMGPATH"));

        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response)
    {
        mListener.onResponse(response);
    }
}

