package com.project.brokerzeye.dbhelper;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.Resources;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class DBHelper {

	public static final String TAG = "DBHelper";
	public static String DATABASE_NAME = "demoproject.sqlite";
	public static final int DATABASE_VERSION = 8;

	public static final String PROFILE_TABLE = "create table if not exists user_profile (userid text, " +
			"name text, phone text, email text, appkey text);";

	public static final String LOGIN_TABLE = "create table if not exists login (login integer, phonenumber text);";

	public static final String ZONE_TABLE = "create table if not exists zone (userid text, " +
			"zoneid text, name text, image text, register text);";

	public static final String PROFESSION_TABLE = "create table if not exists profession (id text, " +
			"name text);";

	private final Context context;
	private DatabaseHelper DBHelper;
	public DBHelper(Context ctx)
	{
		this.context = ctx;
		/*if (isSdPresent()) {
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/ReadyMed");
			if (!dir.exists())
				dir.mkdirs();
			File file = new File(dir, "readymeddemo.sqlite");
			DATABASE_NAME = file.getAbsolutePath();
			Log.e("DataBase Path", DATABASE_NAME);
		}*/
		this.DBHelper = new DatabaseHelper(this.context);
	}
	private class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase db)
		{
			try {
				db.execSQL(PROFILE_TABLE);
				db.execSQL(LOGIN_TABLE);
				db.execSQL(ZONE_TABLE);
				db.execSQL(PROFESSION_TABLE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			//db.execSQL("DROP TABLE IF EXISTS user_profile");
			//db.execSQL("DROP TABLE IF EXISTS login");
			db.execSQL("DROP TABLE IF EXISTS profession");
			onCreate(db);
		}
	}
	//---opens the database---
	public DBHelper open() throws SQLException
	{
		this.DBHelper.getWritableDatabase();
		return this;
	}
	//---closes the database---
	public void close()
	{
		this.DBHelper.close();
	}

	public static boolean isSdPresent() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return false;
		} else {
			return false;
		}
	}

	public String getFileContent(Resources resources, int rawId)
			throws IOException {
		InputStream is = resources.openRawResource(rawId);
		int size = is.available();
		byte[] buffer = new byte[size]; // Read the entire asset into a
		is.read(buffer);
		is.close();
		return new String(buffer, "UTF-8"); // Convert the buffer into a
	}
}

