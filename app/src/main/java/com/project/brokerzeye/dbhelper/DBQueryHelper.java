package com.project.brokerzeye.dbhelper;

import java.util.ArrayList;

import com.project.brokerzeye.model.ProfessionModel;
import com.project.brokerzeye.model.ZonesModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBQueryHelper {

	private final Context context;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;

	public DBQueryHelper(Context ctx)
	{
		this.context = ctx;
	}

	private class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context)
		{
			super(context, DBHelper.DATABASE_NAME, null, DBHelper.DATABASE_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase db)
		{
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
		}
	}
	//---opens the database---
	public DBQueryHelper open() throws SQLException {
		this.dbHelper = new DatabaseHelper(this.context);
		this.db = this.dbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		this.dbHelper.close();
	}

	public long insertProfile(String userid, String name, String phone, String email,
			String appkey){
		ContentValues values = new ContentValues();
		values.put("userid", userid);
		values.put("name", name);
		values.put("phone", phone);
		values.put("email", email);
		values.put("appkey", appkey);
		return db.insert("user_profile", null, values);
	}

	public boolean checkPhoneNumber(String phone){
		boolean flag = false;
		String selectQuery = "select * from user_profile where phone='"+phone+"'";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();

		return flag;

	}

	public boolean checkLogin(String phone, String password){
		boolean flag = false;
		String selectQuery = "select * from user_profile where phone='"+phone+"' and " +
				"password='"+password+"'";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();

		return flag;

	}

	public boolean checkLogin(){
		boolean flag = false;
		String selectQuery = "select * from login";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();

		return flag;
	}

	public String getPhoneNumber(){
		String phnnumber = "";
		String selectQuery = "select phonenumber from login";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			phnnumber = cur.getString(0);
		cur.close();

		return phnnumber;
	}

	public long insertLogin(String phonenumber){
		ContentValues values = new ContentValues();
		values.put("login", 1);
		values.put("phonenumber", phonenumber);
		return db.insert("login", null, values);
	}

	public void deleteLogin(){
		db.delete("login", null, null);
	}

	public void deleteProfile(){
		db.delete("user_profile", null, null);
	}

	public void deleteZones(){
		db.delete("zone", null, null);
	}

	public void deleteZone(String id){
		db.delete("zone", "zoneid='"+id+"'", null);
	}

	public String getUserID(){
		String userid = "";
		String selectQuery = "select userid from user_profile";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			userid = cur.getString(0);
		cur.close();

		return userid;
	}

	public String getAppKey(){
		String appkey = "";
		String selectQuery = "select appkey from user_profile";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			appkey = cur.getString(0);
		cur.close();

		return appkey;
	}

	public long insertZone(String userid, ZonesModel zone){
		ContentValues values = new ContentValues();
		values.put("userid", userid);
		values.put("zoneid", zone.getId());
		values.put("name", zone.getName());
		values.put("image", zone.getImage());
		values.put("register", zone.getRegistered());
		return db.insert("zone", null, values);
	}

	public long updateZone(String userid, ZonesModel zone){
		ContentValues values = new ContentValues();
		values.put("userid", userid);
		values.put("name", zone.getName());
		values.put("image", zone.getImage());
		values.put("register", zone.getRegistered());
		return db.update("zone", values, "zoneid='"+zone.getId()+"'", null);
	}

	public ArrayList<ZonesModel> getZones(){
		ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
		String selectQuery = "select * from zone";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		while(!cur.isAfterLast()){
			ZonesModel zone = new ZonesModel();
			zone.setId(cur.getString(1));
			zone.setName(cur.getString(2));
			zone.setImage(cur.getString(3));
			zone.setRegistered(cur.getString(4));
			zone_list.add(zone);
			cur.moveToNext();
		}
		cur.close();

		return zone_list;
	}

	public long insertProfession(String id, String name){
		ContentValues values = new ContentValues();
		values.put("id", id);
		values.put("name", name);
		return db.insert("profession", null, values);
	}

	public void deleteProfession(){
		db.delete("profession", null, null);
	}

	public ArrayList<ProfessionModel> getProfession(){
		ArrayList<ProfessionModel> pro_list = new ArrayList<ProfessionModel>();
		String selectQuery = "select * from profession";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		while(!cur.isAfterLast()){
			ProfessionModel model = new ProfessionModel();
			model.setId(cur.getString(0));
			model.setName(cur.getString(1));
			model.setSelected(false);
			pro_list.add(model);
			cur.moveToNext();
		}
		cur.close();

		return pro_list;
	}

	public ArrayList<String> getZoneIDList(){
		ArrayList<String> zoneid_list = new ArrayList<String>();
		String selectQuery = "select zoneid from zone";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		while(!cur.isAfterLast()){
			zoneid_list.add(cur.getString(0));
			cur.moveToNext();
		}
		cur.close();

		return zoneid_list;
	}
}
