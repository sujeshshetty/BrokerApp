package com.project.brokerzeye;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.project.brokerzeye.adapter.MenuAdapter;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.fragment.MyZoneFragment;
import com.project.brokerzeye.fragment.RequestZoneFragment;

public class DashBoardActivity extends AppCompatActivity {

	private DrawerLayout mDrawer;
	private LinearLayout slidermenu;
	private FragmentManager manager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		setContentView(R.layout.activity_dashboard);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		slidermenu = (LinearLayout) findViewById(R.id.slidermenu);
		ListView menu_listview = (ListView) findViewById(R.id.menu_listview);
		MenuAdapter menu_adapter = new MenuAdapter(this);
		menu_listview.setAdapter(menu_adapter);
		View menu_footer = LayoutInflater.from(this).inflate(R.layout.menu_footer, menu_listview, false);
		menu_listview.addFooterView(menu_footer);

		final ActionBar ab = getSupportActionBar();
		if (ab != null) {
			ab.setHomeAsUpIndicator(R.drawable.menu_icon);
		}
		ab.setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		menu_listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
									long arg3) {
				// TODO Auto-generated method stub
				if (mDrawer.isDrawerOpen(slidermenu))
					mDrawer.closeDrawer(slidermenu);
				if (position == 6)
					logout();
				else if (position == 1)
					zoneToSelect(1);
				else if(position==4) {
					Intent profileIntent = new Intent(DashBoardActivity.this, ProfileActivity.class);
					startActivity(profileIntent);
				}
			}


		});

		String value;
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			if (bundle.containsKey("requestNewZone")) {
				zoneToSelect(2);
			} else {
				zoneToSelect(1);
			}
		} else {
			zoneToSelect(1);
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mDrawer.isDrawerOpen(slidermenu))
			mDrawer.closeDrawer(slidermenu);
		else
			super.onBackPressed();
	}

	@Override

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home:
				if (mDrawer.isDrawerOpen(slidermenu))
					mDrawer.closeDrawer(slidermenu);
				else
					mDrawer.openDrawer(slidermenu);
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override

	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	}

	private void logout() {
		final AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle("Logout");
		dialog.setMessage("Do you want to logout?");
		dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				DBQueryHelper dbq = new DBQueryHelper(DashBoardActivity.this);
				dbq.open();
				dbq.deleteLogin();
				dbq.deleteProfile();
				dbq.deleteZones();
				dbq.close();
				startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
				finish();
			}
		});
		dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	public void zoneToSelect(int id) {
		if (id == 1) {

			Fragment fragment = new MyZoneFragment();
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transcation = manager.beginTransaction();
			transcation.add(R.id.frame_container, fragment, "MyZoneFragment");
			transcation.commit();


		} else {
			Fragment fragment = new RequestZoneFragment();
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transcation = manager.beginTransaction();
			transcation.add(R.id.frame_container, fragment, "RequestZoneFragment");
			transcation.commit();
		}
	}
}
