package com.project.brokerzeye;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;


public class ApplicationSingleton extends Application {
    private static final String TAG = ApplicationSingleton.class.getSimpleName();

/*    public static final String APP_ID = "27588";
    public static final String AUTH_KEY = "TsFfKHADB5OCMMW";
    public static final String AUTH_SECRET = "Vjy9CEmNS7VaSR7";*/

/*    public static final String APP_ID = "29095";
    public static final String AUTH_KEY = "b5QPnMQ46uMYr9B";
    public static final String AUTH_SECRET = "u4WYjV2-xqLp3fC";*/


    private static ApplicationSingleton instance;
    public static ApplicationSingleton getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate");

        instance = this;

        // Initialise QuickBlox SDK
        //
      //  QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY, AUTH_SECRET);

    }

    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
