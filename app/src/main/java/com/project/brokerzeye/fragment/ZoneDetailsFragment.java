package com.project.brokerzeye.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.project.brokerzeye.R;
import com.project.brokerzeye.TabActivity;
import com.project.brokerzeye.adapter.AddListingAdapter;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.data.DatabaseHandler;
import com.project.brokerzeye.quickbloxtest.core.ChatService;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class ZoneDetailsFragment extends Fragment {
	LinearLayout new_btn;
	AddListingAdapter adapter;
	ArrayList<String> ad_array;
	ListView listviewAd;
	DatabaseHandler databaseHandler;
	MaterialDialog dialog;


	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		ad_array=new ArrayList<String>();
		ChatService.initIfNeed(getActivity());
		databaseHandler = new DatabaseHandler(getActivity());
		final QBUser user = new QBUser();

		user.setLogin(App.getPref().getString("phoneNumber"));
		user.setPassword(App.getPref().getString("phoneNumber"));
		user.setFullName(App.getPref().getString("username"));
		ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
			@Override
			public void onSuccess() {

				Log.e("login", "ok");

				databaseHandler.addUser(App.getPref().getString("phoneNumber"), App.getPref().getString("phoneNumber"));

				Intent intent = new Intent(getActivity(), TabActivity.class);
				startActivity(intent);
				Log.e("login","success");




			}

			@Override
			public void onError(List list) {
				Log.e("login", list.get(0).toString());
				Intent intent = new Intent(getActivity(), TabActivity.class);
				startActivity(intent);

				// progressBar.setVisibility(View.GONE);
				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage("Chat login errors: " + list.get(0).toString()).create().show();

			}
		});
		new_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog = new MaterialDialog.Builder(getActivity())
						.title(null)
						.content("Wating")
						.progress(true, 0)
						.show();

				QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
					@Override
					public void onSuccess(QBSession result, Bundle params) {
						super.onSuccess(result, params);

						final QBUser qbUser = new QBUser(App.getPref().getString("phoneNumber"), App.getPref().getString("phoneNumber"));
						//qbUser.setEmail("piece@gmail.com");
						StringifyArrayList<String> tags = new StringifyArrayList<String>();
						//tags.add(signUpdateOfbith);
						//qbUser.setTags(tags);


						QBUsers.signUp(qbUser, new QBEntityCallbackImpl<QBUser>() {
							@Override
							public void onSuccess(QBUser result, Bundle params) {
								Log.e("sign up ", "ok");
								super.onSuccess(result, params);
								QBUser userLogin = new QBUser();
								userLogin.setLogin(App.getPref().getString("phoneNumber"));
								userLogin.setPassword(App.getPref().getString("phoneNumber"));
								ChatService.getInstance().login(userLogin, new QBEntityCallbackImpl() {


									@Override
									public void onSuccess() {
										if (dialog.isShowing())
											dialog.dismiss();

										databaseHandler.addUser(qbUser.getLogin() != null ? qbUser.getLogin() : qbUser.getEmail(),
												qbUser.getPassword());
										Intent intent = new Intent(getActivity(), TabActivity.class);
										startActivity(intent);

										getActivity().finish();


									}

									@Override
									public void onError(List list) {
										Log.e("login", "no");
										if (dialog.isShowing())
											dialog.dismiss();

										AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
										dialog.setMessage("Chat login errors: " + list.get(0).toString()).create().show();

									}
								});

							}

							@Override
							public void onError(List<String> errors) {
								super.onError(errors);
								if (dialog.isShowing())
									dialog.dismiss();
								AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
								dialog.setMessage("Sign in  errors: " + errors.get(0)).create().show();
								Log.e("error sign up", errors.get(0));

							}
						});
					}

					@Override
					public void onError(List<String> errors) {
						super.onError(errors);
						Log.e("error session", errors.get(0));
						if (dialog.isShowing())
							dialog.dismiss();
					}
				});
				// TODO Auto-generated method stub
				final Dialog dialog = new Dialog(getActivity(), 
						android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.postadd_dialog);
				DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
				int width = metrics.widthPixels;
				dialog.getWindow().setLayout((int)(width/1.1), LayoutParams.WRAP_CONTENT);
				Button dismissButton = (Button) dialog.findViewById(R.id.submit_btn);
				final EditText ettext=(EditText)dialog.findViewById(R.id.ad_details_edt);
				dismissButton.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						String str=ettext.getText().toString();
						PutInArrayList(str);
						dialog.dismiss();
						
					}

					

					
				});
				dialog.show();
			}
		});
	}
	public void PutInArrayList(String str) {
		ad_array.add(str);
		adapter=new AddListingAdapter(getActivity(),ad_array);
		listviewAd.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View vi = inflater.inflate(R.layout.fragment_zonedetails, container, false);
		new_btn = (LinearLayout)vi.findViewById(R.id.new_btn);
		listviewAd=(ListView)vi.findViewById(R.id.list_ad);



		return vi;
	}



}
