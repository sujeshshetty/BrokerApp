package com.project.brokerzeye.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.project.brokerzeye.R;
import com.project.brokerzeye.adapter.RequestZonesAdapter;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.model.ZonesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RequestZoneFragment extends Fragment {

	private String userid = "";
	private ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
	private RequestZonesAdapter adapter;
	private int count = 0, max_count = 0;
	//private ArrayList<String> myzoneid_list = new ArrayList<String>();

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		String url = Util.NEW_ZONE_API+"?userId="+userid;
		GenericAsynTask task = new GenericAsynTask("Sending...", getActivity(), url, null);
		task.setOnTaskListener(new GenericAsynTask.TaskListener() {

			@Override
			public void onSuccess(String success) {
				// TODO Auto-generated method stub
				Log.i("new zone----", success+"");
				try {
					JSONObject res_obj = new JSONObject(success);
					JSONObject status_obj = res_obj.getJSONObject("status");
					if(status_obj.getString("code").equals("200")){
						JSONArray arr = res_obj.getJSONArray("zones");
						zone_list = new ArrayList<ZonesModel>();
						for(int i=0;i<arr.length();i++){
							JSONObject zone_obj = arr.getJSONObject(i);
							ZonesModel zone = new ZonesModel();
							zone.setId(zone_obj.getString("Id"));
							zone.setName(zone_obj.getString("name"));
							zone.setImage(zone_obj.getString("imageLocation"));
							zone.setRegistered(zone_obj.getString("registered"));
							zone.setSelect(false);
							zone_list.add(zone);
						}
						adapter.clear();
						adapter.addAll(zone_list);
						adapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub

			}
		});
		task.execute();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		adapter = new RequestZonesAdapter(getActivity());
		DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		userid = dbq.getUserID();
		//myzoneid_list = dbq.getZoneIDList();
		dbq.close();
		try {
			max_count = Integer.parseInt(Util.readPreference(getActivity(), "max_zone"));
			Log.i("max_count----", max_count+"");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			max_count = 0;
		}
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View vi = inflater.inflate(R.layout.fragment_requestzone, container, false);
		GridView zone_gridview = (GridView)vi.findViewById(R.id.zone_gridview);
		zone_gridview.setAdapter(adapter);
		Button view_zone_btn = (Button)vi.findViewById(R.id.view_zone_btn);
		view_zone_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new MyZoneFragment();
				FragmentManager manager = getActivity().getSupportFragmentManager();
				FragmentTransaction transcation = manager.beginTransaction();
				transcation.replace(R.id.frame_container, fragment, "MyZoneFragment");
				transcation.commit();
			}
		});

		zone_gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(count==max_count)
					showDialog("Error!", "You cann't select more than "+max_count+" zones");
				else{
					ZonesModel model = (ZonesModel) adapter.getItem(arg2);
					if(model.getRegistered().equals("0")){
						if(model.isSelect()){
							count--;
							model.setSelect(false);
						}
						else{
							count++;
							model.setSelect(true);
						}
						adapter.notifyDataSetChanged();
					}
					else
						Toast.makeText(getActivity(), "You are already a member of this zone",
								Toast.LENGTH_SHORT).show();
				}
			}
		});

		Button send_request_btn = (Button)vi.findViewById(R.id.send_request_btn);
		send_request_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String zones_str = "";
				for(ZonesModel model : zone_list){
					if(model.isSelect()){
						if(zones_str.equals(""))
							zones_str = model.getId();
						else
							zones_str = zones_str+","+model.getId();
					}
				}
				if(zones_str.equals(""))
					showDialog("Error!", "Please select zone");
				else{
					if(Connection.checkConnection(getActivity())){
						JSONObject param = new JSONObject();
						try {
							param.put("userId", userid);
							param.put("strZones", zones_str);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("please wait...", 
								getActivity(), Util.SAVE_ZONES_API, param);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("save zone---", success+"");
								try {
									JSONObject obj = new JSONObject(success);
									Toast.makeText(getActivity(), obj.getString("message"), 
											Toast.LENGTH_SHORT).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(getActivity(), "Something went wrong. Please try again", 
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
					else
						Toast.makeText(getActivity(), "Please enable internet connection", 
								Toast.LENGTH_SHORT).show();
				}
			}
		});

		return vi;
	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}
}
