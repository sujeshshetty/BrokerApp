package com.project.brokerzeye.fragment;

/**
 * Created by shetty on 10/3/2015.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.brokerzeye.DashBoardActivity;
import com.project.brokerzeye.LoginActivity;
import com.project.brokerzeye.R;
import com.project.brokerzeye.api.CustomMultiPartEntity;
import com.project.brokerzeye.api.GetApi;
import com.project.brokerzeye.api.MultipartImageRequest;
import com.project.brokerzeye.api.PostApi;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.data.DbClass;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.request.LoginResponse;
import com.project.brokerzeye.utils.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileFragment extends Fragment {

    @Bind(R.id.name)
    MaterialEditText name;
    @Bind(R.id.changePicture)
    ImageView changePicture;
    @Bind(R.id.requestnewzone)
    TextView viewzones;
    @Bind(R.id.viewzones)
    TextView requestnewzone;
    @Bind(R.id.date)
    TextView date;
    /* @Bind(R.id.name)
     EditText name;*/
    @Bind(R.id.zonecount)
    TextView zonecount;
    @Bind(R.id.zonelayout)
    FrameLayout zonelayout;
    @Bind(R.id.adcreated)
    TextView adcreated;
    @Bind(R.id.adtorespond)
    TextView adtorespond;
    @Bind(R.id.profileedit)
    ImageView profileedit;
    @Bind(R.id.submit_btn)
    Button submitBtn;
    private DrawerLayout mDrawer;
    private LinearLayout slidermenu;
    LoginResponse LoginResponse;
    private static final int PICK_IMAGE = 100;

    public static String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static Random rnd = new Random();
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        try {
         //   String gsonTripDetails = getArguments().getString("LoginResponse");
             String gsonTripDetails= App.getPref().getString("gsonTripDetails");

            //   if(gsonTripDetails==null)
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            LoginResponse = gson.fromJson(gsonTripDetails, LoginResponse.class);
            LoginApi();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        // TODO Auto-generated method stub

        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View vi = inflater.inflate(R.layout.activity_profile, container, false);
        ButterKnife.bind(this, vi);
        Button new_zone_btn = (Button) vi.findViewById(R.id.new_zone_btn);
//        Bundle bundle = getActivity().getIntent().getExtras();
        submitBtn.setVisibility(View.GONE);
        name.setFocusableInTouchMode(false);
        name.setFocusable(false);
        name.setHideUnderline(true);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try {

            String gsonTripDetails = getArguments().getString("LoginResponse");
            App.getPref().put("gsonTripDetails",gsonTripDetails);

            if (!gsonTripDetails.equals("") && gsonTripDetails != null) {
                //String gsonTripDetails = bundle.getString("LoginResponse");
                LoginResponse = gson.fromJson(gsonTripDetails, LoginResponse.class);
                App.getPref().put("username", LoginResponse.getUser().getName());
                name.setText(LoginResponse.getUser().getName());
                DateFormat dateFormat = new SimpleDateFormat("MMMM   dd   yyyy");
                Date newDate = new Date();
                System.out.println(dateFormat.format(newDate)); //2014/08/06 15:59:48
                date.setText(dateFormat.format(newDate));
                zonecount.setText("" + LoginResponse.getUser().getZoneCount());
                adcreated.setText("" + LoginResponse.getUser().getAdsCreated());
                adtorespond.setText("" + LoginResponse.getUser().getAdsToRespond());
                Picasso.with(getActivity()).load(LoginResponse.getUser().getProfImgLocation()).fit().centerCrop()
                        .into(changePicture);
                /*Picasso.with(getActivity()).load(  "http://www.networkforgood.com/wp-content/uploads/2015/08/bigstock-Test-word-on-white-keyboard-27134336.jpg").fit().centerCrop()
                        .into(changePicture);*/

                ContentValues appCon = new ContentValues();
                appCon.put(DbClass.COL_PRIMARY_KEY,"1");
                appCon.put(DbClass.PROFILE_NAME, LoginResponse.getUser().getName());
                appCon.put(DbClass.PROFILE_APPKEY, LoginResponse.getUser().getAppkey());
                appCon.put(DbClass.PROFILE_EMAIL,LoginResponse.getUser().getEmailId());
                appCon.put(DbClass.PROFILE_PHONE, LoginResponse.getUser().getPhoneNumber());
                appCon.put(DbClass.PROFILE_USERID, LoginResponse.getUser().getRegId());
                App.getDb().insertUserDetails(appCon, SQLiteDatabase.CONFLICT_REPLACE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vi;
    }
  /*  @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        // supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        slidermenu = (LinearLayout)findViewById(R.id.slidermenu);
        ListView menu_listview = (ListView)findViewById(R.id.menu_listview);
        MenuAdapter menu_adapter = new MenuAdapter(this);
        menu_listview.setAdapter(menu_adapter);
        View menu_footer = LayoutInflater.from(this).inflate(R.layout.menu_footer, menu_listview, false);
        menu_listview.addFooterView(menu_footer);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.menu_icon);
        ab.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        menu_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                if(mDrawer.isDrawerOpen(slidermenu))
                    mDrawer.closeDrawer(slidermenu);
                if(position==6)
                    logout();
            }
        });
        Bundle bundle = getIntent().getExtras();
        String gsonTripDetails = bundle.getString("LoginResponse");
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try {
            LoginResponse = gson.fromJson(gsonTripDetails, LoginResponse.class);
            name.setText(LoginResponse.getUser().getName());
            DateFormat dateFormat = new SimpleDateFormat("MMMM   dd   yyyy");
            Date newDate = new Date();
            System.out.println(dateFormat.format(newDate)); //2014/08/06 15:59:48
            date.setText(dateFormat.format(newDate));
            zonecount.setText("" + LoginResponse.getUser().getZoneCount());
            adcreated.setText("" + LoginResponse.getUser().getAdsCreated());
            adtorespond.setText("" + LoginResponse.getUser().getAdsToRespond());
            Picasso.with(getBaseContext()).load(LoginResponse.getUser().getProfImgLocation()).fit().centerCrop()
                    .into(changePicture);
        }catch(Exception e){
            e.printStackTrace();
        }
       *//* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*//*
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        slidermenu = (LinearLayout) findViewById(R.id.slidermenu);*/
      /*  ListView menu_listview = (ListView)findViewById(R.id.menu_listview);
        MenuAdapter menu_adapter = new MenuAdapter(this);
        menu_listview.setAdapter(menu_adapter);
        View menu_footer = LayoutInflater.from(this).inflate(R.layout.menu_footer, menu_listview, false);
        menu_listview.addFooterView(menu_footer);*/

    /*    final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.menu_icon);
        ab.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        menu_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                if(mDrawer.isDrawerOpen(slidermenu))
                    mDrawer.closeDrawer(slidermenu);
                if(position==6)
                    logout();
            }
        });*/

       /* Fragment fragment = new MyZoneFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transcation = manager.beginTransaction();
        transcation.add(R.id.frame_container, fragment, "MyZoneFragment");
        transcation.commit();*/


    @OnClick(R.id.changePicture)
    public void uploadProfilePicture() {
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK);
        pickImageIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(pickImageIntent, "Select Image"), PICK_IMAGE);

    }

    @OnClick(R.id.profileedit)
    public void zoneLayout() {
        submitBtn.setVisibility(View.VISIBLE);
        name.setFocusableInTouchMode(true);
        name.setFocusable(true);
        name.setHideUnderline(false);
        name.setHint("Enter your name below");
        profileedit.setVisibility(View.GONE);
    }
    @OnClick(R.id.submit_btn)
    public void submitName() {

        if(name.getText().toString().length()>5) {
            updateProfileName();
            profileedit.setVisibility(View.VISIBLE);
            submitBtn.setVisibility(View.GONE);
            name.setFocusableInTouchMode(false);
            name.setFocusable(false);
            name.setHideUnderline(true);
        }
        else
            Toast.makeText(getActivity(),"User name should be greater than 5 characters",Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.viewzones)
    public void viewZones() {
        Intent zoneLayout = new Intent(getActivity(), DashBoardActivity.class);
        startActivity(zoneLayout);

    }

    @OnClick(R.id.requestnewzone)
    public void requestnewzone() {
        Intent zoneLayout = new Intent(getActivity(), DashBoardActivity.class);
        zoneLayout.putExtra("requestNewZone", "requestNewZone");
        startActivity(zoneLayout);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Log.e("on fragment result", "" + requestCode);
        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == PICK_IMAGE) {
            String photoCachePath = getRealPathFromURI(getActivity(), intent.getData());
            // Picasso.with(getBaseContext()).load(new File(photoCachePath)).fit().into(changePicture);
          // sendImageToServer(photoCachePath);
            //new ImageUploader().execute(photoCachePath);
            uploadImageApi(photoCachePath);
          //  uploadProfileApi(photoCachePath);
            //new ImageUploader().execute(photoCachePath);
           /* File imgFile = new  File(photoCachePath);
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                //Drawable d = new BitmapDrawable(getResources(), myBitmap);
                ImageView myImage = (ImageView) findViewById(R.id.changePicture);
                myImage.setImageBitmap(myBitmap);

            }*/

        }
    }
    /*@Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if(mDrawer.isDrawerOpen(slidermenu))
            mDrawer.closeDrawer(slidermenu);
        else
            super.onBackPressed();
    }*/

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawer.isDrawerOpen(slidermenu))
                    mDrawer.closeDrawer(slidermenu);
                else
                    mDrawer.openDrawer(slidermenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void logout() {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setTitle("Logout");
        dialog.setMessage("Do you want to logout?");
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                DBQueryHelper dbq = new DBQueryHelper(getActivity());
                dbq.open();
                dbq.deleteLogin();
                dbq.deleteProfile();
                dbq.close();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] project = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, project, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


    }

    public void uploadImageApi(String photoCachePath) {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/



        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Uploading")
                .content("Please wait")
                .progress(true, 0)
                .show();
        Bitmap bitmap = Utils.resizeBitmap(photoCachePath);

        ByteArrayOutputStream bitmapOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bitmapOutputStream);
        byte[] bitmapToByte = bitmapOutputStream.toByteArray();

        String encodedBitmap = Base64.encodeToString(bitmapToByte, Base64.DEFAULT);
        String randomString=randomString(7);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(LoginResponse.getUser().getUserId()));
        params.put("profImageName", randomString+".jpg");
        params.put("profImage", encodedBitmap);


        String URL = Util.SAVE_PROFILE_API;

        Log.i("", "Upload Profile  url : " + URL);
        Log.e("", "Upload Profile Params: " + new JSONObject(params));
        PostApi api = new PostApi(Request.Method.POST, URL, new JSONObject(params),new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Toast.makeText(getActivity(),"Image Uploaded Successfully",Toast.LENGTH_SHORT).show();
                try {
                    Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                materialDialog.dismiss();
                if(jsonObject.toString().toLowerCase().contains("success"));
                {



                }
                Log.e("", "Upload Profile API RESPONSE RCVD : " + jsonObject);
                try {


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getActivity(),"Image Upload Failed",Toast.LENGTH_SHORT).show();
            }
        });

        App.getVolleyQueue().add(api);
    }
    public void uploadProfileApi(String photoCachePath) {
        Bitmap bitmap = Utils.resizeBitmap(photoCachePath);

        ByteArrayOutputStream bitmapOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bitmapOutputStream);
        byte[] bitmapToByte = bitmapOutputStream.toByteArray();

        String encodedBitmap = Base64.encodeToString(bitmapToByte, Base64.DEFAULT);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(LoginResponse.getUser().getUserId()));
        params.put("profImageName", "image");
        params.put("profImage", encodedBitmap);


        String URL = Util.SAVE_PROFILE_API;
        Log.i("", "Upload Profile  url : " + URL);
        Log.e("", "Upload Profile Params: " + new JSONObject(params));

        PostApi api = new PostApi(Request.Method.POST, URL, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.e("", "Upload Profile API RESPONSE RCVD : " + jsonObject);
                try {
                    String newPhotoCachePath = jsonObject.getString("message");
                    //       CircularImageView newthumbnail=(CircularImageView ) findViewById((R.id.thumbnail_view));

                    // Picasso.with(getBaseContext()).load(newPhotoCachePath).into(newthumbnail);

                    //    Picasso.with(getBaseContext()).load(newPhotoCachePath).error(R.drawable.profile_member_placeholder).into(thumbnail);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.e("", "Volley Error Upload Profile: " + volleyError);

            }
        });

        App.getVolleyQueue().add(api);
    }
    /*@SuppressLint("SimpleDateFormat")
    public void sendImageToServer(final String photoCachePath) {


        Bitmap bitmap = Utils.resizeBitmap(photoCachePath);

        ByteArrayOutputStream bitmapOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 10, bitmapOutputStream);
        byte[] bitmapToByte = bitmapOutputStream.toByteArray();
        String URL = Util.UPDATE_PROFILE_API;
        String encodedBitmap = Base64.encodeToString(bitmapToByte, Base64.DEFAULT);
        HashMap<String, String> params = new HashMap<String, String>();
        String userId = "" + LoginResponse.getUser().getId();
        params.put("userId", userId);
        params.put("profImage", encodedBitmap);
        params.put("profImageName", "image.png");

        // jsonObject.accumulate("mobileNumber", getPhoneNumber());
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Uploading")
                .content("Please wait")
                .progress(true, 0)
                .show();
        Log.e("", "Calling Image Upload URL :" + URL);
        Log.e("", "Params for  :" + params);
        MultipartImageRequest mr = new MultipartImageRequest(URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                materialDialog.dismiss();
                Log.d("response", response);
                try {
                    if ((response != null) && response.contains("`")) {
                        String[] responseParts = response.split("`");
                        String responseStatus = responseParts[0];
                        if (responseStatus.equals("200")) {
                            Picasso.with(getActivity()).load(responseParts[1])
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE).fit()
                                    .into(changePicture);
                            // Picasso.with(getActivity()).invalidate(changePicture);
                            //Picasso.with(getActivity()).load(responseParts[1]).fit().into(changePicture);
                            Toast.makeText(getActivity(), "Image Successfully Uploaded",
                                    Toast.LENGTH_LONG).show();
                        } else {


                            Toast.makeText(getActivity(), "Image Upload failed",
                                    Toast.LENGTH_LONG).show();
                        }


                    } else {

                        Toast.makeText(getActivity(), "Image Upload failed",
                                Toast.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    materialDialog.dismiss();


                    Toast.makeText(getActivity(), "Image Upload Failed",
                            Toast.LENGTH_LONG).show();


                    if (error != null) {
                        Log.e("Volley Request Error", error.getLocalizedMessage());
                        Toast.makeText(getActivity(), error.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        Log.e("Volley Request Error", "Response is Null");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, params);

        Volley.newRequestQueue(getActivity()).add(mr);
    }
*/

    @SuppressLint("SimpleDateFormat")
    public void sendImageToServer(final String photoCachePath) {


        Bitmap bitmap = Utils.resizeBitmap(photoCachePath);

        ByteArrayOutputStream bitmapOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bitmapOutputStream);
        byte[] bitmapToByte = bitmapOutputStream.toByteArray();
        String URL = Util.SAVE_PROFILE_API;
        String encodedBitmap = Base64.encodeToString(bitmapToByte, Base64.DEFAULT);
        HashMap<String, String> params = new HashMap<String, String>();
        String userId = "" + LoginResponse.getUser().getUserId();
        params.put("userId", userId);
        params.put("profImage", encodedBitmap);
        params.put("profImageName", "image.jpeg");

        // jsonObject.accumulate("mobileNumber", getPhoneNumber());
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Uploading")
                .content("Please wait")
                .progress(true, 0)
                .show();
        Log.e("", "Calling Image Upload URL :" + URL);
        Log.e("", "Params for  :" + params);
        MultipartImageRequest mr = new MultipartImageRequest(URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                materialDialog.dismiss();
                Log.d("response", response);
                try {
                    if ((response != null) && response.contains("`")) {
                        String[] responseParts = response.split("`");
                        String responseStatus = responseParts[0];
                        if (responseStatus.equals("200")) {
                            Picasso.with(getActivity()).load(responseParts[1]).fit()
                                    .into(changePicture);
                            // Picasso.with(getActivity()).invalidate(changePicture);
                            //Picasso.with(getActivity()).load(responseParts[1]).fit().into(changePicture);
                            Toast.makeText(getActivity(), "Image Successfully Uploaded",
                                    Toast.LENGTH_LONG).show();
                        } else {


                            Toast.makeText(getActivity(), "Image Upload failed",
                                    Toast.LENGTH_LONG).show();
                        }


                    } else {

                        Toast.makeText(getActivity(), "Image Upload failed",
                                Toast.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    materialDialog.dismiss();


                    Toast.makeText(getActivity(), "Image Upload Failed",
                            Toast.LENGTH_LONG).show();


                    if (error != null) {
                        Log.e("Volley Request Error", error.getLocalizedMessage());
                        Toast.makeText(getActivity(), error.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        Log.e("Volley Request Error", "Response is Null");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, params);

        Volley.newRequestQueue(getActivity()).add(mr);
    }


//}
private class ImageUploader extends AsyncTask<String,Integer,String> {

    long totalSize;
    MaterialDialog materialDialog;
    @Override
    protected void onProgressUpdate(Integer... progress) {

        // TODO Auto-generated method stub


    }

    @SuppressWarnings("deprecation")
    @Override
    protected String doInBackground(String... params) {

        // TODO Auto-generated method stub
        String result = "";
        String actualImageFile = "";
        String imageName = "";
        String imageFile = "";
        // Client-side HTTP transport library
        HttpClient httpClient = new DefaultHttpClient();
        String userId;
        // using POST method
        // HttpPost httpPostRequest = new HttpPost(
        // "http://192.168.0.100:9999/ekrishiservice/imageUpload" );
        HttpPost httpPostRequest = new HttpPost(Util.UPDATE_PROFILE_API);

        try {

            imageFile=params[0];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap compressedBitmap = Utils.resizeBitmap(imageFile);
            compressedBitmap.compress( Bitmap.CompressFormat.JPEG , 10 , baos ); // bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString( b , Base64.DEFAULT );





                CustomMultiPartEntity multiPartEntityBuilder = new CustomMultiPartEntity(new CustomMultiPartEntity.ProgressListener() {

                    @Override
                    public void transferred(long num) {

                        //     publishProgress( ( int ) ( ( num / ( float ) totalSize ) * 100 ) );
                    }
                });

                int strIndex = imageFile.lastIndexOf("/");
                imageName = imageFile.substring(strIndex + 1, imageFile.length());
                String URL = Util.UPDATE_PROFILE_API;

                userId = "" + LoginResponse.getUser().getUserId();

            try {

                multiPartEntityBuilder.addPart("userId", new StringBody(userId));

            multiPartEntityBuilder.addPart("profImage", new StringBody(encodedImage));
                multiPartEntityBuilder.addPart("profImageName", new StringBody("img.jpg"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
                long total = 10;

                httpPostRequest.setEntity(multiPartEntityBuilder);

                // Execute POST request to the given URL
                HttpResponse httpResponse = null;
            String inputStream = null;
            try {


                //inputStream = httpResponse.getEntity().getContent();

                httpResponse = httpClient.execute(httpPostRequest);
                // receive response as inputStream
               // String inputStream = null;
             //   inputStream = httpResponse.getEntity().getContent();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


                //inputStream = httpResponse.getEntity().getContent();

            String responseStatus = httpResponse.getStatusLine().toString();

            if (responseStatus.contains("200")) {
                final String response = httpResponse.getFirstHeader( "RESPONSE" ).getValue();
                inputStream = httpResponse.getFirstHeader( "IMGPATH" ).getValue();
                inputStream = inputStream.replaceAll(" ", "%20");
                Picasso.with(getActivity()).load(inputStream).fit()
                        .into(changePicture);
                // Picasso.with(getActivity()).invalidate(changePicture);
                //Picasso.with(getActivity()).load(responseParts[1]).fit().into(changePicture);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), response,
                                Toast.LENGTH_LONG).show();
                    }//public void run() {
                });

            } else {

                final String response = httpResponse.getFirstHeader( "RESPONSE" ).getValue();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), response,
                                Toast.LENGTH_LONG).show();
                    }//public void run() {
                });
            }







        } catch (Exception e) {
e.printStackTrace();

            return null;
        }

        return result;
    }

    @Override
    protected void onPreExecute() {

        // TODO Auto-generated method stub
        super.onPreExecute();
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Uploading")
                .content("Please wait")
                .progress(true, 0)
                .show();

    }


    // uploadStatus.setText( "Uploading image to server" );


    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);
        materialDialog.dismiss();

        // uploadStatus.setText( result );
    }

}


public void LoginApi() {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
final String phnnumber_str = App.getPref().getString("phoneNumber");
final String password_str = App.getPref().getString("password");
final String regId = App.getPref().getString("regId");
final HashMap<String, String> params = new HashMap<String, String>();

        params.put("phoneNumber", phnnumber_str);
        params.put("password", password_str);
        params.put("regId", regId);

        //String hardcodedDomain= "http://172.16.1.94:8080/TelematicsRESTService/services/ServiceProcess/";
        String URL = "http://122.166.212.177:7777/dubaiapp/rest/loginservice/login";//+ Constants.UPLOAD_BILLS;
        Log.i("", "Login  url : " + URL);
        Log.e("", "Login Params: " + new JSONObject(params));
        PostApi api = new PostApi(Request.Method.POST, URL, new JSONObject(params), new Response.Listener<JSONObject>() {
@Override
public void onResponse(JSONObject jsonObject) {

        Log.e("", "Login API RESPONSE RCVD : " + jsonObject);
        try {






            GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        LoginResponse getTripDetailsResponse = gson.fromJson(String.valueOf(jsonObject), LoginResponse.class);
                   /* JSONObject res_obj = new JSONObject(success);
                    JSONObject status_obj = res_obj.getJSONObject("status");
                    String res_code = status_obj.getString("code");
                    String message = status_obj.getString("message");*/
        // Toast.makeText(getActivity(), getTripDetailsResponse.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
        if (getTripDetailsResponse.getStatus().getCode() == 200) {
        //  JSONObject user_obj = res_obj.getJSONObject("user");

        name.setText(getTripDetailsResponse.getUser().getName());
        DateFormat dateFormat = new SimpleDateFormat("MMMM   dd   yyyy");
        Date newDate = new Date();
        System.out.println(dateFormat.format(newDate)); //2014/08/06 15:59:48
        date.setText(dateFormat.format(newDate));
        zonecount.setText("" + getTripDetailsResponse.getUser().getZoneCount());
        adcreated.setText("" + getTripDetailsResponse.getUser().getAdsCreated());
        adtorespond.setText("" + getTripDetailsResponse.getUser().getAdsToRespond());
            Picasso.with(getActivity()).load(getTripDetailsResponse.getUser().getProfImgLocation()).fit().centerCrop()
                    .into(changePicture);
           /* Picasso.with(getActivity()).load(  "http://www.networkforgood.com/wp-content/uploads/2015/08/bigstock-Test-word-on-white-keyboard-27134336.jpg").fit().centerCrop()
                    .into(changePicture);*/

        }

        } catch (Exception e) {
        e.printStackTrace();
        }
        }
        }, new Response.ErrorListener() {
@Override
public void onErrorResponse(VolleyError volleyError) {
        String gsonTripDetails= App.getPref().getString("gsonTripDetails");
        //   if(gsonTripDetails==null)
        if(gsonTripDetails!=null) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        LoginResponse = gson.fromJson(gsonTripDetails, LoginResponse.class);
        name.setText(LoginResponse.getUser().getName());
        DateFormat dateFormat = new SimpleDateFormat("MMMM   dd   yyyy");
        Date newDate = new Date();
        System.out.println(dateFormat.format(newDate)); //2014/08/06 15:59:48
        date.setText(dateFormat.format(newDate));
        zonecount.setText("" + LoginResponse.getUser().getZoneCount());
        adcreated.setText("" + LoginResponse.getUser().getAdsCreated());
        adtorespond.setText("" + LoginResponse.getUser().getAdsToRespond());
        Log.e("", "Volley Error Login: " + volleyError);
        }
        }
        });

        App.getVolleyQueue().add(api);
        }

public void updateProfileName() {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        String userId = "" + LoginResponse.getUser().getUserId();
        //String hardcodedDomain= "http://172.16.1.94:8080/TelematicsRESTService/services/ServiceProcess/";
        String encoded_params = null;
        try {
        encoded_params = URLEncoder.encode(name.getText().toString(), "UTF-8");

        } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
        }

        String URL = "http://122.166.212.177:7777/dubaiapp/rest/userservice/updateUserName?userId="+userId+"&userName="+encoded_params;//+ Constants.UPLOAD_BILLS;

        GetApi updateProfileApi = new GetApi(
        new Response.Listener<JSONObject>() {
@Override
public void onResponse(JSONObject jsonObject) {

        Log.e("", "Update Profile API RESPONSE RCVD : " + jsonObject);
        try {
        Toast.makeText(getActivity(), "Updated name successfully", Toast.LENGTH_SHORT).show();





        } catch (Exception e) {
        e.printStackTrace();
        }
        }
        }, new Response.ErrorListener() {
@Override
public void onErrorResponse(VolleyError volleyError) {

        Log.e("", "Volley Error Login: " + volleyError);
        Toast.makeText(getActivity(), "Failed to update name", Toast.LENGTH_SHORT).show();
        }
        },URL);

        App.getVolleyQueue().add(updateProfileApi);
        }






    String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

        }







