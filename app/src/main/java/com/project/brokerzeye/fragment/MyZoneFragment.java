package com.project.brokerzeye.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.project.brokerzeye.R;
import com.project.brokerzeye.adapter.ZonesAdapter;
import com.project.brokerzeye.api.GetApi;
import com.project.brokerzeye.app.App;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.data.DatabaseHandler;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.model.ZonesModel;
import com.project.brokerzeye.quickbloxtest.core.ChatService;
import com.project.brokerzeye.quickbloxtest.ui.activities.SplashActivity;
import com.project.brokerzeye.utils.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyZoneFragment extends Fragment {

	private ZonesAdapter adapter;
	private String userid = "";
	private ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
	private boolean destroy = false;
	DatabaseHandler databaseHandler;

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
        ChatService.initIfNeed(getActivity());
		final DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		zone_list = dbq.getZones();
		String msg = "";
		if(zone_list.size()==0)
			msg = "Loading...";
		else{
			adapter.clear();
			adapter.addAll(zone_list);
			adapter.notifyDataSetChanged();
		}
		String url = Util.USER_ZONES_API+"?userId="+userid;
		GenericAsynTask task = new GenericAsynTask(msg, getActivity(), url, null);
		task.setOnTaskListener(new GenericAsynTask.TaskListener() {

			@Override
			public void onSuccess(String success) {
				// TODO Auto-generated method stub
				Log.i("zones----", success+"");
				if (!destroy) {
				try {
					JSONObject res_obj = new JSONObject(success);
					JSONObject status_obj = res_obj.getJSONObject("status");
					if(status_obj.getString("code").equals("200")){
						String max_zone = res_obj.getString("maxZoneCount");
						if(getActivity()!=null)
							Util.writePreference(getActivity(), "max_zone", max_zone);
						JSONArray arr = res_obj.getJSONArray("zones");
						zone_list = new ArrayList<ZonesModel>();
						for(int i=0;i<arr.length();i++){
							JSONObject zone_obj = arr.getJSONObject(i);
							ZonesModel zone = new ZonesModel();
							zone.setId(zone_obj.getString("Id"));
							zone.setName(zone_obj.getString("name"));
							zone.setImage(zone_obj.getString("imageLocation"));
							zone.setRegistered(zone_obj.getString("registered"));
							long id = dbq.updateZone(userid, zone);
							if(id==0)
								dbq.insertZone(userid, zone);
							zone_list.add(zone);
						}
						adapter.clear();
						adapter.addAll(zone_list);
						adapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dbq.close();
			}
			}

			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub

			}
		});
		task.execute();
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		destroy = true;
		super.onDestroyView();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		adapter = new ZonesAdapter(getActivity());
		DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		userid = dbq.getUserID();
		dbq.close();
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View vi = inflater.inflate(R.layout.fragment_myzone, container, false);
		GridView zone_gridview = (GridView)vi.findViewById(R.id.zone_gridview);
		zone_gridview.setAdapter(adapter);
		zone_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
                                    long arg3) {
				//checkPhoneNumber();
				Intent intent = new Intent(getActivity(), SplashActivity.class);
				intent.putExtra("zoneId", zone_list.get(arg2).getId());
				Const.zoneId = zone_list.get(arg2).getId();
				startActivity(intent);
				// TODO Auto-generated method stub
				/*Fragment fragment = new ZoneDetailsFragment();
				FragmentManager manager = getActivity().getSupportFragmentManager();
				FragmentTransaction transcation = manager.beginTransaction();
				transcation.replace(R.id.frame_container, fragment, "ZoneDetailsFragment");
				transcation.commit();*/
				/*ChatService.initIfNeed(getActivity());
				databaseHandler = new DatabaseHandler(getActivity());
				final QBUser user = new QBUser();
				if (ChatService.getInstance().getCurrentUser() != null) {
					Intent intent = new Intent(getActivity(), TabActivity.class);
					intent.putExtra("zoneId", zone_list.get(arg2).getId());
					Const.zoneId = zone_list.get(arg2).getId();
					startActivity(intent);
				} else
				{
					user.setLogin(App.getPref().getString("phoneNumber"));
				user.setPassword(App.getPref().getString("phoneNumber"));
				user.setFullName(App.getPref().getString("username"));
				ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
					@Override
					public void onSuccess() {

						Log.e("login", "ok");

						databaseHandler.addUser(App.getPref().getString("phoneNumber"), App.getPref().getString("phoneNumber"));

						Intent intent = new Intent(getActivity(), TabActivity.class);
						intent.putExtra("zoneId", zone_list.get(arg2).getId());
						Const.zoneId = zone_list.get(arg2).getId();
						startActivity(intent);
						Log.e("login", "success");


					}

					@Override
					public void onError(List list) {
						Log.e("login", list.get(0).toString());
						*//*Intent intent = new Intent(getActivity(), TabActivity.class);
						startActivity(intent);*//*

						// progressBar.setVisibility(View.GONE);
						AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
						dialog.setMessage("Chat login errors: " + list.get(0).toString()).create().show();

					}
				});
			}*/

            }
        });

		Button new_zone_btn = (Button)vi.findViewById(R.id.new_zone_btn);
		new_zone_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new RequestZoneFragment();
				FragmentManager manager = getActivity().getSupportFragmentManager();
				FragmentTransaction transcation = manager.beginTransaction();
				transcation.replace(R.id.frame_container, fragment, "RequestZoneFragment");
				transcation.commit();
			}
		});

		Button remove_zone_btn = (Button)vi.findViewById(R.id.remove_zone_btn);
		remove_zone_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Fragment fragment = new DeleteZoneFragment();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transcation = manager.beginTransaction();
                transcation.replace(R.id.frame_container, fragment, "DeleteZoneFragment");
                transcation.commit();
            }
        });

		return vi;
	}


    public void checkPhoneNumber() {
       /* try {
           // String encoded_params = URLEncoder.encode(encodedBitmap, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/


        //String hardcodedDomain= "http://172.16.1.94:8080/TelematicsRESTService/services/ServiceProcess/";


        String URL = "http://122.166.212.177:7777/dubaiapp/rest/userservice/checkPhoneNumber?phoneNumber="+ App.getPref().getString("phoneNumber");//+ Constants.UPLOAD_BILLS;
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Connecting to chat")
                .content("Please wait")
                .progress(true, 0)
                .show();
        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
						Intent intent=new Intent(getActivity(), SplashActivity.class);
						startActivity(intent);
                        Log.e("", "Update Profile API RESPONSE RCVD : " + jsonObject);
                        /*try {

							databaseHandler = new DatabaseHandler(getActivity());
							final QBUser user = new QBUser();

							user.setLogin(App.getPref().getString("phoneNumber"));
							user.setPassword(App.getPref().getString("phoneNumber"));
							user.setFullName(App.getPref().getString("username"));
							ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
								@Override
								public void onSuccess() {
                                    if(materialDialog!=null)
                                        materialDialog.dismiss();
									Log.e("login", "ok");

									databaseHandler.addUser(App.getPref().getString("phoneNumber"), App.getPref().getString("phoneNumber"));

									Intent intent = new Intent(getActivity(), TabActivity.class);
									startActivity(intent);
									Log.e("login","success");




								}

								@Override
								public void onError(List list) {
                                    if(materialDialog!=null)
                                        materialDialog.dismiss();
									Log.e("login", list.get(0).toString());
									Intent intent = new Intent(getActivity(), TabActivity.class);
									startActivity(intent);

									// progressBar.setVisibility(View.GONE);
									AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
									dialog.setMessage("Chat login errors: " + list.get(0).toString()).create().show();

								}
							});




                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if(materialDialog!=null)
                    materialDialog.dismiss();
                Log.e("", "Volley Error Login: " + volleyError);


            }
        },URL);

        App.getVolleyQueue().add(updateProfileApi);
    }
}
