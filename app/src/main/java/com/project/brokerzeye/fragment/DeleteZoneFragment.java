package com.project.brokerzeye.fragment;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.project.brokerzeye.R;
import com.project.brokerzeye.adapter.DeleteZonesAdapter;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.model.ZonesModel;

public class DeleteZoneFragment extends Fragment {

	private DeleteZonesAdapter adapter;
	private String userid = "";
	private ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
	private String zones_str = "";

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setView();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		adapter = new DeleteZonesAdapter(getActivity());
		DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		userid = dbq.getUserID();
		dbq.close();
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View vi = inflater.inflate(R.layout.fragment_deletezone, container, false);
		GridView zone_gridview = (GridView)vi.findViewById(R.id.zone_gridview);
		zone_gridview.setAdapter(adapter);
		zone_gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				ZonesModel model = (ZonesModel) adapter.getItem(arg2);
				if(model.isSelect()){
					model.setSelect(false);
				}
				else{
					model.setSelect(true);
				}
				adapter.notifyDataSetChanged();
			}
		});

		Button view_zone_btn = (Button)vi.findViewById(R.id.view_zone_btn);
		view_zone_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new MyZoneFragment();
				FragmentManager manager = getActivity().getSupportFragmentManager();
				FragmentTransaction transcation = manager.beginTransaction();
				transcation.replace(R.id.frame_container, fragment, "MyZoneFragment");
				transcation.commit();
			}
		});

		Button delete_btn = (Button)vi.findViewById(R.id.delete_btn);
		delete_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				zones_str = "";
				for(ZonesModel model : zone_list){
					if(model.isSelect()){
						if(zones_str.equals(""))
							zones_str = model.getId();
						else
							zones_str = zones_str+","+model.getId();
					}
				}
				if(zones_str.equals(""))
					showDialog("Error!", "Please select zone");
				else{
					AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
					alert.setMessage("Do you want to delete selected zone?");
					alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							deleteZone();
						}
					});
					alert.setNegativeButton("NO", null);
					alert.show();
				}
			}
		});

		return vi;
	}

	private void deleteZone(){
		if(Connection.checkConnection(getActivity())){
			JSONObject param = new JSONObject();
			try {
				param.put("userId", userid);
				param.put("strZones", zones_str);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			GenericAsynTask task = new GenericAsynTask("please wait...", 
					getActivity(), Util.DELETE_ZONES_API, param);
			task.setOnTaskListener(new GenericAsynTask.TaskListener() {

				@Override
				public void onSuccess(String success) {
					// TODO Auto-generated method stub
					Log.i("delete zone---", success+"");
					try {
						JSONObject obj = new JSONObject(success);
						JSONObject status_obj = obj.getJSONObject("status");
						Toast.makeText(getActivity(), status_obj.getString("message"), 
								Toast.LENGTH_SHORT).show();
						if(status_obj.getString("code").equals("200")){
							DBQueryHelper dbq = new DBQueryHelper(getActivity());
							dbq.open();
							for(ZonesModel model : zone_list){
								if(model.isSelect()){
									dbq.deleteZone(model.getId());
								}
							}
							dbq.close();
							setView();
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(String error) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "Something went wrong. Please try again", 
							Toast.LENGTH_SHORT).show();
				}
			});
			task.execute();
		}
		else
			Toast.makeText(getActivity(), "Please enable internet connection", 
					Toast.LENGTH_SHORT).show();
	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}

	private void setView(){
		zone_list.clear();
		final DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		zone_list = dbq.getZones();
		adapter.clear();
		adapter.addAll(zone_list);
		adapter.notifyDataSetChanged();
		dbq.close();
	}
}
