package com.project.brokerzeye.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDexApplication;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.project.brokerzeye.data.DbClass;
import com.project.brokerzeye.data.Pref;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBSettings;

import io.fabric.sdk.android.Fabric;


/**
 * Created by vijay on 27/5/15.
 */
public class App extends MultiDexApplication {

    private static RequestQueue volleyQueue;
    private static Context appContext;
    private static DbClass db;

    static Pref pref;


  /*  public static final String APP_ID = "27588";
    public static final String AUTH_KEY = "TsFfKHADB5OCMMW";
    public static final String AUTH_SECRET = "Vjy9CEmNS7VaSR7";*/

   /* public static final String APP_ID = "29095";
    public static final String AUTH_KEY = "b5QPnMQ46uMYr9B";
    public static final String AUTH_SECRET = "u4WYjV2-xqLp3fC";*/
   private static final String TAG = App.class.getSimpleName();


    public static final String STICKER_API_KEY = "847b82c49db21ecec88c510e377b452c";

    public static final String USER_LOGIN = "05612345679";
    public static final String USER_PASSWORD = "05612345679";


    public static final String APP_ID = "29576";
    public static final String AUTH_KEY = "GUuBR3BwONFTnah";
    public static final String AUTH_SECRET = "fyQVAN5uMSSSSrh";


    private static App instance;
    public static App getInstance() {
        return instance;
    }

    public static int version =0;
    @Override
    public void onCreate() {

        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        initApplication();

        // Initialise QuickBlox SDK
        //
     // QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY, AUTH_SECRET);
        appContext = this;
        try {

            PackageInfo pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (Exception e) {
            version=0;
        }


        volleyQueue = Volley.newRequestQueue(getApplicationContext());


    }




    private void initImageLoader(Context context) {
        //ImageLoader.getInstance().init(ImageUtils.getImageLoaderConfiguration(context));
    }





    public static RequestQueue getVolleyQueue() {
        if (volleyQueue == null) {
            volleyQueue = Volley.newRequestQueue(getAppContext());
        }
        return volleyQueue;
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static DbClass getDb() {

        if (db == null) {
            db = new DbClass(getAppContext());
            db.initialize();
        }
        return db;
    }

    public static Pref getPref() {
        if (pref == null) {
            pref = new Pref(getAppContext());
        }
        return pref;
    }


    private void initApplication() {
        instance = this;
        QBChatService.setDebugEnabled(true);
        initImageLoader(this);
        QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY,
                AUTH_SECRET);



      /*  QBSettings.getInstance().fastConfigInit(Consts.QB_APP_ID, Consts.QB_AUTH_KEY,
                Consts.QB_AUTH_SECRET);*/
        //soundPlayer = new MediaPlayerManager(this);

    }
    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

}