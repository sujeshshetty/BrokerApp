package com.project.brokerzeye;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.project.brokerzeye.adapter.MenuAdapter;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.fragment.ProfileFragment;
import com.project.brokerzeye.utils.Const;
import com.project.brokerzeye.quickbloxtest.ui.activities.SplashActivity;
/**
 * Created by shetty on 9/30/2015.
 */
public class ProfileActivity extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private LinearLayout slidermenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        //supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        slidermenu = (LinearLayout) findViewById(R.id.slidermenu);
        ListView menu_listview = (ListView) findViewById(R.id.menu_listview);
        MenuAdapter menu_adapter = new MenuAdapter(this);
        menu_listview.setAdapter(menu_adapter);
        View menu_footer = LayoutInflater.from(this).inflate(R.layout.menu_footer, menu_listview, false);
        menu_listview.addFooterView(menu_footer);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_36dp);
        ab.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        menu_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (mDrawer.isDrawerOpen(slidermenu))
                    mDrawer.closeDrawer(slidermenu);
                if (position == 6)
                    logout();
                if (position == 2) {
                    Intent intent = new Intent(ProfileActivity.this, SplashActivity.class);
                    intent.putExtra("zoneId", "0");
                    Const.zoneId = "0";
                    startActivity(intent);
                }
                if (position == 3) {
                    Intent intent = new Intent(ProfileActivity.this, SplashActivity.class);

                    Const.zoneId = "0";
                    startActivity(intent);
                }
                else if (position == 1)
                    zoneToSelect(1);

            }
        });

        Fragment fragment = new ProfileFragment();
        fragment.setArguments(getIntent().getExtras());
        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction transcation = manager.beginTransaction();

        transcation.add(R.id.frame_container, fragment, "MyProfileFragment");
        transcation.commit();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (mDrawer.isDrawerOpen(slidermenu))
            mDrawer.closeDrawer(slidermenu);
        else
            super.onBackPressed();
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawer.isDrawerOpen(slidermenu))
                    mDrawer.closeDrawer(slidermenu);
                else
                    mDrawer.openDrawer(slidermenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void logout() {
        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle("Logout");
        dialog.setMessage("Do you want to logout?");
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                DBQueryHelper dbq = new DBQueryHelper(ProfileActivity.this);
                dbq.open();
                dbq.deleteLogin();
                dbq.deleteProfile();
                dbq.close();
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
            }
        });
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void zoneToSelect(int id) {
        if (id == 1) {

          Intent intent = new Intent(ProfileActivity.this,DashBoardActivity.class);
            startActivity(intent);

        }
    }
}