package com.project.brokerzeye.common;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

@SuppressWarnings("deprecation")
public class Connection {

	public static boolean checkConnection(Context ctx) 
	{
		ConnectivityManager conMgr =  (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;

		return true;
	}

	public static String serverCallGet(String Url) 
	{
		Url= Url.replaceAll(" ", "%20");
		Log.d("url---", Url+">>");
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response;
		String jsonstring="";
		try{
			HttpGet post = new HttpGet(Url);
			response = client.execute(post);

			if(response!=null){
				InputStream in = response.getEntity().getContent();
				BufferedInputStream bis = new BufferedInputStream(in);
				ByteArrayBuffer baf = new ByteArrayBuffer(20);

				int current = 0;

				while((current = bis.read()) != -1){
					baf.append((byte)current);
				} 
				jsonstring = new String(baf.toByteArray());
			}
		}catch(Exception e){
			e.printStackTrace();
			String error = e.getMessage();
			return error;
		}

		return jsonstring;
	}

	public static String serverCallPost(String Url, JSONObject parameter) 
	{
		Log.d("url---", Url+">>");
		Log.v("parameter---", parameter.toString());
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		HttpResponse response;
		String jsonstring="";
		try{
			HttpPost post = new HttpPost(Url);
			StringEntity se = new StringEntity(parameter.toString(), "UTF-8");

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if(response!=null){
				InputStream in = response.getEntity().getContent();
				BufferedInputStream bis = new BufferedInputStream(in);
				ByteArrayBuffer baf = new ByteArrayBuffer(20);

				int current = 0;

				while((current = bis.read()) != -1){
					baf.append((byte)current);
				}
				jsonstring = new String(baf.toByteArray());
			}
		}catch(Exception e){
			e.printStackTrace();

		}

		return jsonstring;
	}
	
	public static InputStream OpenHttpConnection(String urlString) throws IOException {
		InputStream in = null;
		int response = -1;

		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();

		if (!(conn instanceof HttpURLConnection))
			throw new IOException("Not an HTTP connection");

		HttpURLConnection httpConn = (HttpURLConnection) conn;
		httpConn.setAllowUserInteraction(false);
		httpConn.setInstanceFollowRedirects(true);
		httpConn.setRequestMethod("GET");
		httpConn.connect();

		response = httpConn.getResponseCode();
		if (response == HttpURLConnection.HTTP_OK) {
			in = httpConn.getInputStream();
		}

		return in;
	}
	
}
