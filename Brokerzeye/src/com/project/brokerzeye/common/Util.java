package com.project.brokerzeye.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.project.brokerzeye.SplashActivity;

public class Util {

	/**
	 * API
	 */
	public static final String SERVER_URL = "http://122.166.212.177:8888/dubaiapp/rest/";
	public static final String LOGIN_API = SERVER_URL+"loginservice/login";
	public static final String REGISTRATION_API = SERVER_URL+"loginservice/register";
	public static final String FORGOT_PASSWORD_API = SERVER_URL+"loginservice/forgotPwd";
	public static final String UPDATE_GCM_API = SERVER_URL+"loginservice/updateGCMId";
	public static final String CHANGE_PASSWORD_API = SERVER_URL+"loginservice/changePwd";
	public static final String USER_ZONES_API = SERVER_URL+"zoneservice/userZones";
	public static final String NEW_ZONE_API = SERVER_URL+"zoneservice/requestNewZones";
	
	/**
	 * GCM
	 */
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String SENDER_ID = "556696427388";
	
	@SuppressLint("NewApi")
	public static String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("Demo", "Registration not found.");
			return "";
		}

		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			return "";
		}
		return registrationId;
	}
	
	public static SharedPreferences getGCMPreferences(Context context) {

		return context.getSharedPreferences(SplashActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}
	
	/**
	 * Shared Preference
	 */
	
	public static void writePreference(Context mctx, String key, String value)
	{
		SharedPreferences prefs = mctx.getSharedPreferences("demo_preference", 0);
		SharedPreferences.Editor editor= prefs.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String readPreference(Context mctx, String key)
	{
		SharedPreferences prefs = mctx.getSharedPreferences("demo_preference", 0);
		String value= prefs.getString(key, "");
		return value;
	}
	
	
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean ValidatePhoneNumber(String phone) {
		String[] format={"\\d{3}-\\d{3}-\\d{4}","\\(\\d{3}\\)\\ \\d{3}-\\d{4}","\\d{3}\\ \\d{3}\\ \\d{4}","\\d{3}.\\d{3}.\\d{4}","\\d{10}"};

		for(int s=0;s<format.length;s++)
		{
			Pattern pattern = Pattern.compile(format[s]);
			Matcher matcher = pattern.matcher(phone);
			if (matcher.matches()) 
				return true;
		}
		return false;
	}
}
