package com.project.brokerzeye.common;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class GenericAsynTask extends AsyncTask<String, Void, String>{

	private TaskListener taskListener;
	private String dialogMsg="", url="";
	private ProgressDialog pd;
	private Context context;
	private JSONObject param;

	public GenericAsynTask(String dialogMsg, Context context, String url, JSONObject params) {
		this.dialogMsg = dialogMsg;
		this.context = context;
		this.url = url;
		this.param = params;
	}

	@Override
	protected void onPreExecute() {
		if(dialogMsg!=null && !dialogMsg.equals(""))
			pd = ProgressDialog.show(context, "", dialogMsg);
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String response="";
		if(param!=null)
			response = Connection.serverCallPost(url, param);
		else
			response = Connection.serverCallGet(url);
		
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		Log.v("result----", result+">>>");
		if(result!=null && !result.equals(""))
			taskListener.onSuccess(result);
		else
			taskListener.onFailure(result);
		if(pd!=null)
			pd.dismiss();
	}

	public static interface TaskListener {

		public void onSuccess(String success);

		public void onFailure(String error);
	}

	public void setOnTaskListener(TaskListener listener) {

		this.taskListener = listener;
	}
}
