package com.project.brokerzeye.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.project.brokerzeye.R;
import com.project.brokerzeye.adapter.ZonesAdapter;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.model.ZonesModel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

public class MyZoneFragment extends Fragment {

	private ZonesAdapter adapter;
	private String userid = "";
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		String url = Util.NEW_ZONE_API+"?userId="+userid;
		GenericAsynTask task = new GenericAsynTask("Loading...", getActivity(), url, null);
		task.setOnTaskListener(new GenericAsynTask.TaskListener() {
			
			@Override
			public void onSuccess(String success) {
				// TODO Auto-generated method stub
				Log.i("zones----", success+"");
				ArrayList<ZonesModel> zone_list = new ArrayList<ZonesModel>();
				try {
					JSONObject res_obj = new JSONObject(success);
					JSONObject status_obj = res_obj.getJSONObject("status");
					if(status_obj.getString("code").equals("200")){
						JSONArray arr = res_obj.getJSONArray("zones");
						for(int i=0;i<arr.length();i++){
							JSONObject zone_obj = arr.getJSONObject(i);
							ZonesModel zone = new ZonesModel();
							zone.setId(zone_obj.getString("Id"));
							zone.setName(zone_obj.getString("name"));
							zone.setImage(zone_obj.getString("imageLocation"));
							zone.setRegistered(zone_obj.getString("registered"));
							zone_list.add(zone);
						}
						adapter.clear();
						adapter.addAll(zone_list);
						adapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				
			}
		});
		task.execute();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		adapter = new ZonesAdapter(getActivity());
		DBQueryHelper dbq = new DBQueryHelper(getActivity());
		dbq.open();
		userid = dbq.getUserID();
		dbq.close();
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View vi = inflater.inflate(R.layout.fragment_myzone, container, false);
		GridView zone_gridview = (GridView)vi.findViewById(R.id.zone_gridview);
		zone_gridview.setAdapter(adapter);
		Button new_zone_btn = (Button)vi.findViewById(R.id.new_zone_btn);
		new_zone_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requestNewZone();
			}
		});
		
		return vi;
	}

	private void requestNewZone(){
		String url = Util.NEW_ZONE_API+"?userId="+userid;
		GenericAsynTask task = new GenericAsynTask("Sending...", getActivity(), url, null);
		task.setOnTaskListener(new GenericAsynTask.TaskListener() {
			
			@Override
			public void onSuccess(String success) {
				// TODO Auto-generated method stub
				Log.i("new zone----", success+"");
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				
			}
		});
		task.execute();
	}
}
