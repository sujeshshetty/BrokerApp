package com.project.brokerzeye;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.project.brokerzeye.adapter.MenuAdapter;
import com.project.brokerzeye.dbhelper.DBQueryHelper;
import com.project.brokerzeye.fragment.MyZoneFragment;

public class DashBoardActivity extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private LinearLayout slidermenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dashboard);

		slidermenu = (LinearLayout)findViewById(R.id.slidermenu);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		ListView menu_listview = (ListView)findViewById(R.id.menu_listview);
		MenuAdapter menu_adapter = new MenuAdapter(this);
		menu_listview.setAdapter(menu_adapter);
		ImageButton menu_btn = (ImageButton)findViewById(R.id.menu_btn);
		menu_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mDrawerLayout.isDrawerOpen(slidermenu))
					mDrawerLayout.closeDrawer(slidermenu);
				else
					mDrawerLayout.openDrawer(slidermenu);
			}
		});

		menu_listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(mDrawerLayout.isDrawerOpen(slidermenu))
					mDrawerLayout.closeDrawer(slidermenu);
				if(arg2==6){
					logout();
				}
			}
		});
		
		Fragment fragment = new MyZoneFragment();
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transcation = manager.beginTransaction();
		transcation.add(R.id.frame_container, fragment, "MyZoneFragment");
		transcation.commit();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(mDrawerLayout.isDrawerOpen(slidermenu))
			mDrawerLayout.closeDrawer(slidermenu);
		else
			super.onBackPressed();
	}
	
	private void logout(){
		final AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle("Logout");
		dialog.setMessage("Do you want to logout?");
		dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				DBQueryHelper dbq = new DBQueryHelper(DashBoardActivity.this);
				dbq.open();
				dbq.deleteLogin();
				dbq.close();
				finish();
			}
		});
		dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
