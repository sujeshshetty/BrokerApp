package com.project.brokerzeye;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.common.GenericAsynTask.TaskListener;
import com.project.brokerzeye.dbhelper.DBQueryHelper;

public class LoginActivity extends Activity implements TextWatcher {

	private EditText phonenumber_edt, password_edt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);

		phonenumber_edt = (EditText)findViewById(R.id.phonenumber_edt);
		phonenumber_edt.addTextChangedListener(this);
		password_edt = (EditText)findViewById(R.id.password_edt);
		password_edt.addTextChangedListener(this);

		Button signup_btn = (Button)findViewById(R.id.signup_btn);
		signup_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
			}
		});

		Button signin_btn = (Button)findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(LoginActivity.this)){
					final String phnnumber_str = phonenumber_edt.getText().toString().trim();
					String password_str = password_edt.getText().toString().trim();
					String regId = Util.getRegistrationId(LoginActivity.this);
					if(!Util.ValidatePhoneNumber(phnnumber_str))
						phonenumber_edt.setError("please enter valid phone number");
					else if(password_str.equals(""))
						password_edt.setError("please enter password");
					else if(regId==null || regId.equals(""))
						showDialog("Error!", "Your device is not properly setup for this application");
					else{
						JSONObject obj = new JSONObject();
						try {
							obj.put("phoneNumber", phnnumber_str);
							obj.put("password", password_str);
							obj.put("regId", regId);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("Authenticating...", LoginActivity.this, 
								Util.LOGIN_API, obj);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("login---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									JSONObject status_obj = res_obj.getJSONObject("status");
									String res_code = status_obj.getString("code");
									String message = status_obj.getString("message");
									JSONObject user_obj = res_obj.getJSONObject("user");
									Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
									if(res_code.equals("200")){
										DBQueryHelper dbq = new DBQueryHelper(LoginActivity.this);
										dbq.open();
										dbq.insertLogin(phnnumber_str);
										dbq.insertProfile(user_obj.getString("Id"), user_obj.getString("name"), 
												user_obj.getString("phoneNumber"), user_obj.getString("emailId"),
												user_obj.getString("appkey"));
										dbq.close();
										Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
										startActivity(intent);
										finish();
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(LoginActivity.this, "Something went wrong. Please try again", 
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(LoginActivity.this, "Please enable internet connection", 
							Toast.LENGTH_SHORT).show();
			}
		});

		Button forgot_pass_btn = (Button)findViewById(R.id.forgot_pass_btn);
		forgot_pass_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showForgetPassDialog();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		phonenumber_edt.setError(null);
		password_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}

	private void showForgetPassDialog(){
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.forgot_pass_dialog);
		dialog.setTitle("Forgot Password?");
		final EditText phnnumber_edt = (EditText)dialog.findViewById(R.id.phnnumber_edt);
		Button submit_btn = (Button)dialog.findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(LoginActivity.this)){
					String phonenmuber_str = phnnumber_edt.getText().toString().trim();
					if(!Util.ValidatePhoneNumber(phonenmuber_str))
						phnnumber_edt.setError("please enter valid phone number");
					else{
						String url = Util.FORGOT_PASSWORD_API+"?phoneNumber="+phonenmuber_str;
						GenericAsynTask task = new GenericAsynTask("Please wait...", LoginActivity.this, 
								url, null);
						task.setOnTaskListener(new TaskListener() {
							
							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("forgot pass---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									String res_code = res_obj.getString("code");
									String message = res_obj.getString("message");
									if(res_code.equals("200")){
										Util.writePreference(LoginActivity.this, 
												"forgot_pass", "1");
										startActivity(new Intent(LoginActivity.this, 
												ChangePasswordActivity.class));
										dialog.dismiss();
										finish();
									}
									Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(LoginActivity.this, "Something went wrong. Please try again", 
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(LoginActivity.this, "Please enable internet connection", 
							Toast.LENGTH_SHORT).show();
			}
		});
		dialog.show();
	}
}
