package com.project.brokerzeye;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBHelper;
import com.project.brokerzeye.dbhelper.DBQueryHelper;

public class SplashActivity extends Activity {

	private String regId = "";
	private GoogleCloudMessaging gcm;
	public static final String EXTRA_MESSAGE = "message";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN );
		setContentView(R.layout.activity_splash);

		DBHelper db = new DBHelper(this);
		db.open();
		db.close();

		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regId = Util.getRegistrationId(this);

			if (regId.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i("Demo", "No valid Google Play Services APK found.");
		}

		gotoNextScreen(1000);
	}

	private void gotoNextScreen(long delay){
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
				dbq.open();
				if(dbq.checkLogin())
					startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
				else{
					if(Util.readPreference(SplashActivity.this, "forgot_pass").equals("1"))
						startActivity(new Intent(SplashActivity.this, ChangePasswordActivity.class));
					else
						startActivity(new Intent(SplashActivity.this, LoginActivity.class));
				}
				dbq.close();
				finish();
			}
		}, delay);
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("Demo", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private void registerInBackground() {

		new GetRegistrationId().execute();
	}

	private class GetRegistrationId extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(SplashActivity.this);
				}
				regId = gcm.register(Util.SENDER_ID);
				msg = "Device registered, registration ID=" + regId;

				storeRegistrationId(SplashActivity.this, regId);
				DBQueryHelper dbq = new DBQueryHelper(SplashActivity.this);
				dbq.open();
				if(dbq.checkLogin()){
					String phnnumber = dbq.getPhoneNumber();
					String appkey = dbq.getAppKey();
					String url = Util.UPDATE_GCM_API+"?phoneNumber="+phnnumber+"&appkey="+appkey+
							"&regId="+regId;
					String up_result = Connection.serverCallGet(url);
					Log.v("up_result---", up_result+"");
				}
				dbq.close();
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
			}

			return msg;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = Util.getGCMPreferences(context);
		int appVersion = Util.getAppVersion(context);
		Log.i("Demo", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Util.PROPERTY_REG_ID, regId);
		editor.putInt(Util.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
}
