package com.project.brokerzeye.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBQueryHelper {

	private final Context context;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;

	public DBQueryHelper(Context ctx)
	{
		this.context = ctx;
	}

	private class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context)
		{
			super(context, DBHelper.DATABASE_NAME, null, DBHelper.DATABASE_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase db)
		{
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
		}
	}
	//---opens the database---
	public DBQueryHelper open() throws SQLException {
		this.dbHelper = new DatabaseHelper(this.context);
		this.db = this.dbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		this.dbHelper.close();
	}
	
	public long insertProfile(String userid, String name, String phone, String email, 
			String appkey){
		ContentValues values = new ContentValues();
		values.put("userid", userid);
		values.put("name", name);
		values.put("phone", phone);
		values.put("email", email);
		values.put("appkey", appkey);
		return db.insert("user_profile", null, values);
	}
	
	public boolean checkPhoneNumber(String phone){
		boolean flag = false;
		String selectQuery = "select * from user_profile where phone='"+phone+"'";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();
		
		return flag;
		
	}
	
	public boolean checkLogin(String phone, String password){
		boolean flag = false;
		String selectQuery = "select * from user_profile where phone='"+phone+"' and " +
				"password='"+password+"'";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();
		
		return flag;
		
	}
	
	public boolean checkLogin(){
		boolean flag = false;
		String selectQuery = "select * from login";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			flag = true;
		cur.close();
		
		return flag;
	}
	
	public String getPhoneNumber(){
		String phnnumber = "";
		String selectQuery = "select phonenumber from login";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			phnnumber = cur.getString(0);
		cur.close();
		
		return phnnumber;
	}
	
	public long insertLogin(String phonenumber){
		ContentValues values = new ContentValues();
		values.put("login", 1);
		values.put("phonenumber", phonenumber);
		return db.insert("login", null, values);
	}
	
	public void deleteLogin(){
		db.delete("login", null, null);
	}
	
	public String getUserID(){
		String userid = "";
		String selectQuery = "select userid from user_profile";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			userid = cur.getString(0);
		cur.close();
		
		return userid;
	}
	
	public String getAppKey(){
		String appkey = "";
		String selectQuery = "select appkey from user_profile";
		Cursor cur = db.rawQuery(selectQuery, null);
		cur.moveToFirst();
		if(cur.getCount()>0)
			appkey = cur.getString(0);
		cur.close();
		
		return appkey;
	}
}
