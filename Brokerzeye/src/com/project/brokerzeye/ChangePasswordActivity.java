package com.project.brokerzeye;

import org.json.JSONException;
import org.json.JSONObject;

import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;
import com.project.brokerzeye.dbhelper.DBQueryHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePasswordActivity extends Activity implements TextWatcher {

	private EditText phnnumber_edt, newpass_edt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_changepassword);
		
		phnnumber_edt = (EditText)findViewById(R.id.phnnumber_edt);
		phnnumber_edt.addTextChangedListener(this);
		newpass_edt = (EditText)findViewById(R.id.newpass_edt);
		newpass_edt.addTextChangedListener(this);
		
		Button submit_btn = (Button)findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(ChangePasswordActivity.this)){
					String phnnumber_str = phnnumber_edt.getText().toString().trim();
					String new_password_str = newpass_edt.getText().toString().trim();
					if(!Util.ValidatePhoneNumber(phnnumber_str))
						phnnumber_edt.setError("please enter valid phone number");
					else if(new_password_str.equals(""))
						newpass_edt.setError("please enter new password");
					else{
						DBQueryHelper dbq = new DBQueryHelper(ChangePasswordActivity.this);
						dbq.open();
						String appkey = dbq.getAppKey();
						dbq.close();
						JSONObject obj = new JSONObject();
						try {
							obj.put("phoneNumber", phnnumber_str);
							obj.put("newPassword", new_password_str);
							obj.put("appkey", appkey);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("Please wait...", ChangePasswordActivity.this, 
								Util.CHANGE_PASSWORD_API, obj);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {
							
							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("change pass---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									String res_code = res_obj.getString("code");
									String message = res_obj.getString("message");
									if(res_code.equals("200")){
										Util.writePreference(ChangePasswordActivity.this, 
												"forgot_pass", "");
										startActivity(new Intent(ChangePasswordActivity.this, 
												LoginActivity.class));
										finish();
									}
									Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_SHORT).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(ChangePasswordActivity.this, "Please enable internet connection", 
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		phnnumber_edt.setError(null);
		newpass_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}

}
