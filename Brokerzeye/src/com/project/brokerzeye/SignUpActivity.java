package com.project.brokerzeye;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project.brokerzeye.common.Connection;
import com.project.brokerzeye.common.GenericAsynTask;
import com.project.brokerzeye.common.Util;

public class SignUpActivity extends Activity implements TextWatcher {

	private EditText name_edt, email_edt, password_edt, phone_edt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_signup);

		name_edt = (EditText)findViewById(R.id.name_edt);
		name_edt.addTextChangedListener(this);
		email_edt = (EditText)findViewById(R.id.email_edt);
		email_edt.addTextChangedListener(this);
		password_edt = (EditText)findViewById(R.id.password_edt);
		password_edt.addTextChangedListener(this);
		phone_edt = (EditText)findViewById(R.id.phone_edt);
		phone_edt.addTextChangedListener(this);

		Button signin_btn = (Button)findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Button join_btn = (Button)findViewById(R.id.join_btn);
		join_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Connection.checkConnection(SignUpActivity.this)){
					String name_str = name_edt.getText().toString().trim();
					String email_str = email_edt.getText().toString().trim();
					String password_str = password_edt.getText().toString().trim();
					String phone_str = phone_edt.getText().toString().trim();
					String regId = Util.getRegistrationId(SignUpActivity.this);
					if(name_str.equals(""))
						name_edt.setError("please enter name");
					else if(email_str.equals(""))
						email_edt.setError("please enter email");
					else if(password_str.equals(""))
						password_edt.setError("please enter password");
					else if(phone_str.equals(""))
						phone_edt.setError("please enter phone number");
					else if(regId==null || regId.equals(""))
						showDialog("Error!", "Your device is not properly setup for this application");
					else{
						JSONObject obj = new JSONObject();
						try {
							obj.put("name", name_str);
							obj.put("password", password_str);
							obj.put("phoneNumber", phone_str);
							obj.put("emailId", email_str);
							obj.put("regId", regId);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						GenericAsynTask task = new GenericAsynTask("Please wait...", SignUpActivity.this, 
								Util.REGISTRATION_API, obj);
						task.setOnTaskListener(new GenericAsynTask.TaskListener() {

							@Override
							public void onSuccess(String success) {
								// TODO Auto-generated method stub
								Log.i("registration---", success+"");
								try {
									JSONObject res_obj = new JSONObject(success);
									JSONObject status_obj = res_obj.getJSONObject("status");
									String res_code = status_obj.getString("code");
									String message = status_obj.getString("message");
									Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
									if(res_code.equals("200")){
										finish();
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onFailure(String error) {
								// TODO Auto-generated method stub
								Toast.makeText(SignUpActivity.this, "Something went wrong. Please try again", 
										Toast.LENGTH_SHORT).show();
							}
						});
						task.execute();
					}
				}
				else
					Toast.makeText(SignUpActivity.this, "Please enable internet connection", 
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		name_edt.setError(null);
		email_edt.setError(null);
		password_edt.setError(null);
		phone_edt.setError(null);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	private void showDialog(String title, String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(title);
		dialog.setMessage(msg);
		dialog.setPositiveButton("OK", null);
		dialog.show();
	}
}
